import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class Passenger {
  String name;
  String surname;
  String passportId;

  Passenger({
    required this.name,
    required this.surname,
    required this.passportId,
  });

  @override
  String toString() {
    return '[name: $name, surname: $surname, passportId: $passportId]';
  }

  Map<String, dynamic> toJson() => {
    'name': name,
    'surname': surname,
    'passportId': passportId,
  };

  // Convert Map to Passenger object
  factory Passenger.fromJson(Map<String, dynamic> json) => Passenger(
    name: json['name'],
    surname: json['surname'],
    passportId: json['passport_id'],
  );
}

class FlightStore with ChangeNotifier {
  Map<String, dynamic>? _flight;
  List<Passenger> _passengers = [];

  Map<String, dynamic>? get flight => _flight;
  List<Passenger> get passengers => _passengers;

  void setFlight(Map<String, dynamic> flight) {
    _flight = flight;
    notifyListeners();
  }

  void addPassenger(Passenger passenger) {
    _passengers.add(passenger);
    notifyListeners();
  }

  void clearPassengers() {
    _passengers = [];
    notifyListeners();
  }

  void removePassenger(Passenger passenger) {
    _passengers.remove(passenger);
    notifyListeners();
  }

  void clearFlightDetails() {
    _flight = null;
    _passengers.clear();
    notifyListeners();
  }
}