import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';


class User {
  final int id;
  final String name;
  final String email;
  final String surname;
  final String passportId;

  User({
    required this.id,
    required this.name,
    required this.email,
    required this.surname,
    required this.passportId,
  });

  // Convert User object to Map
  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'email': email,
    'surname': surname,
    'passportId': passportId,
  };

  // Convert Map to User object
  factory User.fromJson(Map<String, dynamic> json) => User(
    id: json['id'],
    name: json['name'],
    email: json['email'],
    surname: json['surname'],
    passportId: json['passportId'],
  );

  @override
  String toString() {
    return 'User[id: $id, name: $name, surname: $surname, passportId: $passportId]';
  }
}

class UserStore with ChangeNotifier {
  User? _user;

  User? get user => _user;

  Future<void> setUser(User user) async {
    _user = user;
    notifyListeners();
    await _saveUserToPrefs(user);
  }

  Future<void> clearUser() async {
    _user = null;
    notifyListeners();
    await _clearUserFromPrefs();
  }

  Future<void> _saveUserToPrefs(User user) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user', jsonEncode(user.toJson()));
  }

  Future<void> _clearUserFromPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('user');
  }

  Future<void> loadUserFromPrefs() async {
    final prefs = await SharedPreferences.getInstance();
    final userJson = prefs.getString('user');
    if (userJson != null) {
      _user = User.fromJson(jsonDecode(userJson));
      notifyListeners();
    }
  }
}