import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:senior_project/store/user_store.dart';
import 'package:flutter_masked_text2/flutter_masked_text2.dart';

import '../../helper/api.dart';
import '../../store/flight_store.dart';
import '../tabs_screen.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({Key? key}) : super(key: key);

  @override
  State<PaymentScreen> createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  var _date = new MaskedTextController(mask: '00/00');
  bool validateCardInfo() {
    return true;
  }

  String? _token;

  fetchToken() async {
    await getAuth().then((value) {
      print('value in get auth $value');
      _token = value;
    });
    calculateEndTime();
  }

  void redirectToNext() {
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => const TabsScreen(startingIndex: 1)),
    // );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Confirmation'),
          content: Text('Payment has been processed.'),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const TabsScreen(startingIndex: 1)),
                );
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> handleConfirm(BuildContext context) async {
    if (validateCardInfo()) {
      // if validation is ok
      // send request to backend
      final flightStore = Provider.of<FlightStore>(context, listen: false);
      final userStore = Provider.of<UserStore>(context, listen: false);
      final user = userStore.user;
      final flight = flightStore.flight;
      var customDeptPort = {};
      dynamic customDestPort = {};

      print('userStore ${user}');
      print('flightStore ${flight}');
      print('passengers ${flightStore.passengers}');

      if (flight!['deptCoordinates'] != null) {
        final Uri urlPort = Uri.parse("$baseUrl/ports");

        flight!['deptCoordinates']['name'] =
            flight!['deptCoordinates']['name'].toString().substring(0, 12);

        var responseTime = await http.post(
          urlPort,
          body: jsonEncode(
            flight!['deptCoordinates'],
          ),
          headers: {
            'Authorization': _token!,
            'Content-Type': 'application/json'
          },
        );
        customDeptPort = jsonDecode(responseTime.body);
        print('customDeptPort $customDeptPort');
      }

      if (flight!['destCoordinates'] != null) {
        final Uri urlPort = Uri.parse("$baseUrl/ports");

        flight!['destCoordinates']['name'] =
            flight!['destCoordinates']['name'].toString().substring(0, 12);

        var responseTime = await http.post(
          urlPort,
          body: jsonEncode(
            flight!['destCoordinates'],
          ),
          headers: {
            'Authorization': _token!,
            'Content-Type': 'application/json'
          },
        );
        customDestPort = jsonDecode(responseTime.body);
        print('customDestPort $customDestPort');
      }
      var body = flight!['id'] != '-1'
          ? {
              "itineraryId": flightStore.flight!['flightId'].toString(),
              "userId": user?.id.toString(),
              "passengers":
                  flightStore.passengers.map((p) => p.toJson()).toList(),
              "amountPaid": flight?['price'].toString()
            }
          : {
              "userId": user?.id.toString(),
              "passengerList":
                  flightStore.passengers.map((p) => p.toJson()).toList(),
              "departPortId": flight!['deptCoordinates'] == null
                  ? flight!['departure']['id']
                  : customDeptPort['id'],
              "destPortId": flight!['destCoordinates'] == null
                  ? flight!['destination']['id']
                  : customDestPort['id'],
              "isShared": flight!['isShared'],
              "departDate": flight!['departureTime'], //.split('T')[1],
              "destDate": flight!['destinationTime'], //.split('T')[1],
            };
      print('body $body');
      // return;
      final Uri url = flight!['id'] != '-1'
          ? Uri.parse("$baseUrl/bookings")
          : Uri.parse("$baseUrl/itineraries");
      var response = await http.post(
        url,
        body: jsonEncode(body),
        headers: {'Authorization': _token!, 'Content-Type': 'application/json'},
      );
      print('-------response');
      print(response.body);
      if (response.statusCode == 200) {
        redirectToNext();
      }
    } else {
      // if validation is not ok
    }
  }

  calculateEndTime() async {
    final flightStore = Provider.of<FlightStore>(context, listen: false);
    final flight = flightStore.flight;

    print('flight in end time $flight');

    if (flight!['id'] == '-1') {
      final Uri urlTime = Uri.parse("$baseUrl/itineraries/destination-time");
      var params = {
        "depX": flight!['deptCoordinates'] == null
            ? flight!['departure']['latitude'].toString()
            : flight!['deptCoordinates']['latitude'].toString(),
        "depY": flight!['deptCoordinates'] == null
            ? flight!['departure']['longitude'].toString()
            : flight!['deptCoordinates']['longitude'].toString(),
        "destX": flight!['destCoordinates'] == null
            ? flight!['destination']['latitude'].toString()
            : flight!['destCoordinates']['latitude'].toString(),
        "destY": flight!['destCoordinates'] == null
            ? flight!['destination']['longitude'].toString()
            : flight!['destCoordinates']['longitude'].toString(),
        'departDate': flight!['departureTime'].toString().replaceAll('T', ' '),
      };
      var responseTime = await http.get(
        urlTime.replace(queryParameters: params),
        headers: {'Authorization': _token!, 'Content-Type': 'application/json'},
      );
      setState(() {
        flight['destinationTime'] = responseTime.body.replaceAll('"', "");
      });

      print('query ${params}');
      print('responseTime ${responseTime.body.replaceAll('"', "")}');
    }
  }

  @override
  void initState() {
    super.initState();
    fetchToken();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    final flightStore = Provider.of<FlightStore>(context);
    final flight = flightStore.flight;
    print(flight);
    flight!['price'] = flightStore.passengers.length * 125;

    // print('flight \n $flight');

    return Container(
      color: Color.fromRGBO(249, 251, 250, 1),
      child: Scaffold(
        appBar: AppBar(
          elevation: 2,
          backgroundColor: Color.fromRGBO(249, 251, 250, 1),
          centerTitle: true,
          title: Text(
            'Payment',
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: flight == null
            ? Center(child: Text('No flight details available'))
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Card(
                      elevation: 3,
                      color: Color.fromRGBO(238, 239, 239, 1),
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 32.0, vertical: 24.0),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  flight['flightId'] != null
                                      ? 'flight #${flight['flightId']}'
                                      : 'new flight',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Icon(Icons.calendar_month_outlined),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      '${flight['departureTime'].split('T')[0]}',
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: Divider(
                                color: Color.fromRGBO(132, 182, 248, 1),
                              ),
                            ),
                            Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            '${flight['departureTime'].split('T')[1].substring(0, 5)}',
                                            style: TextStyle(
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            flight!['deptCoordinates'] == null
                                                ? '${flight!['departureName'].toString().characters.take(8).toString()}(${flight!['departureCode']})'
                                                : flight!['deptCoordinates']
                                                        ['name']
                                                    .toString()
                                                    .characters
                                                    .take(8)
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Flexible(
                                        child: Image.asset(
                                            'lib/assets/images/bookings.png',
                                            width: 150),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            '${flight['destinationTime'].split('T')[1].substring(0, 5)}',
                                            style: TextStyle(
                                                fontSize: 22,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            flight!['destCoordinates'] == null
                                                ? '${flight!['destinationName'].toString().characters.take(8).toString()}(${flight!['destinationCode']})'
                                                : flight!['destCoordinates']
                                                        ['name']
                                                    .toString()
                                                    .characters
                                                    .take(8)
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Divider(color: Color.fromRGBO(132, 182, 248, 1)),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Total',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Text(
                                    flight['price'] != null
                                        ? '\$${flight['price']}'
                                        : '\$1000',
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontWeight: FontWeight.w600,
                                        color:
                                            Color.fromRGBO(132, 182, 248, 1)),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(32, 8, 32, 8),
                      child: Form(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TextFormField(
                              inputFormatters: [
                                FilteringTextInputFormatter.digitsOnly,
                                CardNumberFormatter(),
                                LengthLimitingTextInputFormatter(19)
                              ],
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                labelText: 'Card number',
                                labelStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                                hintText: '5300 0000 0000 0000',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12,
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue),
                                ),
                              ),
                            ),
                            SizedBox(height: 20),
                            TextFormField(
                              keyboardType: TextInputType.name,
                              decoration: const InputDecoration(
                                labelText: 'Card holder name',
                                labelStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                                hintText: 'Aruzhan Zhunusbekova',
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12,
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide.none,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue),
                                ),
                              ),
                            ),
                            SizedBox(height: 15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  width: (screenWidth - 32 * 3) / 2,
                                  child: TextFormField(
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(3)
                                    ],
                                    keyboardType: TextInputType.number,
                                    decoration: const InputDecoration(
                                      labelText: 'CVV',
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                      hintText: '000',
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 12,
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide.none,
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.grey),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.blue),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(width: 32),
                                SizedBox(
                                  width: (screenWidth - 32 * 3) / 2,
                                  child: TextFormField(
                                    controller: _date,
                                    inputFormatters: [
                                      LengthLimitingTextInputFormatter(5)
                                    ],
                                    decoration: const InputDecoration(
                                      labelText: 'Expiry date',
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                      hintText: '05/24',
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 12,
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide.none,
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.grey),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.blue),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      color: Colors.white,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 32, vertical: 0),
                      height: 60,
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => Color.fromRGBO(132, 182, 248, 1)),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          )),
                        ),
                        onPressed: () {
                          handleConfirm(context);
                        },
                        child: const Text(
                          'Confirm',
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    Container(
                      color: Colors.white,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 32, vertical: 0),
                      height: 60,
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(
                              width: 1,
                              color: Color(0xFF84B6F8),
                            ),
                          ),
                        ),
                        onPressed: () {},
                        child: const Text(
                          'Cancel',
                          style: TextStyle(
                              color: Color(0xFF84B6F8),
                              letterSpacing: 1,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}

class CardNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue previousValue,
    TextEditingValue nextValue,
  ) {
    var inputText = nextValue.text;

    if (nextValue.selection.baseOffset == 0) {
      return nextValue;
    }

    var bufferString = StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue % 4 == 0 && nonZeroIndexValue != inputText.length) {
        bufferString.write(' ');
      }
    }

    var string = bufferString.toString();
    return nextValue.copyWith(
      text: string,
      selection: TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}
