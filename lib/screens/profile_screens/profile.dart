import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  double spaceBetweenInputs = 22.0;

  var information = [
    InfoItem("Name", "Aruzhan", const Icon(Icons.portrait)),
    InfoItem("Surname", "Zhunusbekova", const Icon(Icons.portrait)),
    InfoItem("Email address", "aruzhanish@gmail.com",
        const Icon(Icons.email_outlined)),
    InfoItem("Passport ID", "ED 25265 589", const Icon(Icons.fingerprint))
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                  color: Colors.black,
                  icon: const Icon(Icons.arrow_back_ios_new),
                  onPressed: () {
                    context.go("/login");
                  },
                ),
                const Text('Personal Information',
                    style: TextStyle(color: Colors.black)),
                IconButton(
                  color: Colors.black,
                  icon: const Icon(Icons.edit_outlined),
                  onPressed: () {
                    context.go("/profileEdit");
                  },
                ),

                // ),
              ]),
          centerTitle: true,
          surfaceTintColor: Colors.white,
        ),
        body: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      "Hello. Aruzhan!",
                      style: TextStyle(
                        fontSize: 20,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    for (var info in information)
                      Container(
                        margin: EdgeInsets.only(top: spaceBetweenInputs),
                        child: Row(
                          children: [
                            info.icon,
                            Text(
                              "  ${info.title}: ${info.placeholder}",
                              textAlign: TextAlign.start,
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ]),
        ));
  }
}

class InfoItem {
  InfoItem(
      [this.title = '',
      this.placeholder = '',
      this.icon = const Icon(Icons.email)]);
  String title;
  String placeholder;
  Icon icon;
}
