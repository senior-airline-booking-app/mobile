import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../store/user_store.dart';
// import 'package:senior_project/screens/screens_from_tabs/profile_screen.dart';

class ProfileEditPage extends StatefulWidget {
  const ProfileEditPage({super.key});

  @override
  State<ProfileEditPage> createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  double spaceBetweenInputs = 22.0;


  @override
  Widget build(BuildContext context) {
    final userStore = Provider.of<UserStore>(context);
    final user = userStore.user;

    var information = [
      InfoItem("Name", user!.name, const Icon(Icons.portrait)),
      InfoItem("Surname", user!.surname, const Icon(Icons.portrait)),
      InfoItem("Email address", user!.email,
          const Icon(Icons.email_outlined)),
      InfoItem("Passport ID", user!.passportId, const Icon(Icons.fingerprint))
    ];

    return Scaffold(
        appBar: AppBar(
          leading: const BackButton(color: Colors.black),
          backgroundColor: Colors.white,
          title: const Text(
            'Personal Information',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          surfaceTintColor: Colors.white,
        ),
        body: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      for (var info in information)
                        Container(
                          margin: EdgeInsets.only(top: spaceBetweenInputs),
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintStyle: const TextStyle(color: Colors.grey),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              prefixIcon: info.icon,
                              labelText: info.title,
                              floatingLabelAlignment:
                                  FloatingLabelAlignment.start,
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              hintText: info.placeholder,
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return 'required';
                              }
                              return null;
                            },
                          ),
                        ),
                      // const SizedBox(height: 15),
                    ],
                  ),
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Color.fromRGBO(132, 182, 248, 1),
                    minimumSize: Size(100, 60),
                    padding: EdgeInsets.all(16.0),
                    textStyle: TextStyle(),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Center(
                    child: Text(
                      'Save',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ]),
        ));
  }
}

class InfoItem {
  InfoItem(
      [this.title = '',
      this.placeholder = '',
      this.icon = const Icon(Icons.email)]);
  String title;
  String placeholder;
  Icon icon;
}
