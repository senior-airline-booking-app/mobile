import 'dart:async';

import 'package:flutter/material.dart';
import 'package:senior_project/screens/authorization_screens/login.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navigateFurther();
  }

  _navigateFurther() async {
    Timer(
        const Duration(seconds: 1),
        () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const LoginPage()))
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: const BoxDecoration(
            color: Color(0xff84B6F8),
            image: DecorationImage(
              image: AssetImage("lib/assets/images/splash_background.png"),
              fit: BoxFit.cover,
              opacity: 450,
            ),
          ),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Image(
                  image: AssetImage("lib/assets/images/splash_plane_icon.png")),
              Text(
                'SeaBookApp',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 36,
                    fontWeight: FontWeight.bold),
              ),
            ],
          ))),
    );
  }
}
