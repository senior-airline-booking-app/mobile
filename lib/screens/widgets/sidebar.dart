import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/authorization_screens/login.dart';
import 'package:senior_project/screens/tabs_screen.dart';

import '../../store/user_store.dart';

class SidebarMenu extends StatefulWidget {
  const SidebarMenu({Key? key}) : super(key: key);

  @override
  State<SidebarMenu> createState() => _SidebarMenuState();
}

class _SidebarMenuState extends State<SidebarMenu> {
  var name = 'Aruzhan';
  var surname = 'Zhunusbekova';

  void redirectToUserProfile() {
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          return const TabsScreen(startingIndex: 4);
        },
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      ),
    );

    // на случай если вам не залетит без анимации
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const TabsScreen(startingIndex: 4,)));
  }

  void redirectToMyBookings() {
    Navigator.pushReplacement(
      context,
      PageRouteBuilder(
        pageBuilder: (BuildContext context, Animation<double> animation1,
            Animation<double> animation2) {
          return const TabsScreen(startingIndex: 1);
        },
        transitionDuration: Duration.zero,
        reverseTransitionDuration: Duration.zero,
      ),
    );

    // на случай если вам не залетит без анимации
    // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>const TabsScreen(startingIndex: 1,)));
  }

  Future<void> logout(BuildContext context) async {
    final userStore = Provider.of<UserStore>(context, listen: false);
    await userStore.clearUser(); // Clear user from store and local storage

    // Navigate to the login page and remove all previous routes
    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => LoginPage()),
      (route) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    final userStore = Provider.of<UserStore>(context);
    final user = userStore.user;

    return ListView(
      // Important: Remove any padding from the ListView.
      padding: const EdgeInsets.symmetric(horizontal: 16),
      children: [
        SizedBox(
          height: 200,
          child: DrawerHeader(
            child: Stack(
              children: [
                SizedBox(
                  width: 100,
                  child: IconButton(
                      icon: const Icon(Icons.close),
                      tooltip: 'Increase volume by 10',
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      splashRadius: 1,
                      padding: EdgeInsets.zero,
                      alignment: Alignment.topLeft,
                      style: IconButton.styleFrom()),
                ),
                Positioned(
                  bottom: 8.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Hello,",
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      Text(
                        '${user?.name} ${user?.surname}',
                        style: TextStyle(color: Colors.black, fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        ListTile(
          leading: const Icon(Icons.info_outline),
          title: const Text('About App'),
          onTap: () {
            // redirectToUserProfile();
          },
        ),
        ListTile(
          leading: const Icon(Icons.question_answer_outlined),
          title: const Text('FAQ'),
          onTap: () {
            // redirectToMyBookings();
          },
        ),
        const Divider(),
        ListTile(
          leading: const Icon(Icons.logout_outlined),
          title: const Text('Logout'),
          onTap: () {
            logout(context);
          },
        ),
        // const ListTile(
        //   title: Text(
        //     'App version 1.0.1',
        //     style: TextStyle(color: Colors.grey),
        //   ),
        // ),
      ],
    );
  }
}
