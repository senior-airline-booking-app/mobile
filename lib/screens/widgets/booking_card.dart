import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../screens_from_tabs/my_bookings_screens/booking_details_screen.dart';


class BookingCard extends StatefulWidget {
  final Object texts;
  const BookingCard({Key? key, required this.texts}) : super(key: key);

  @override
  State<BookingCard> createState() => _BookingCardState();
}

class _BookingCardState extends State<BookingCard> {


  Color _greyColor = Color.fromRGBO(85, 85, 85, 1);
  Object data = {'startTime': '', 'endTime': '', 'date': ''};

  initState(){
    DateTime dateTime = DateTime.parse((widget.texts as Map)["startTime"]);
    String output = DateFormat('dd/MM/yyyy').format(dateTime);
    (data as Map)["date"] = output;
    String time = DateFormat('HH:mm').format(dateTime);
    (data as Map)["startDate"]= time;
    dateTime = DateTime.parse((widget.texts as Map)["endTime"]);
    time = DateFormat('HH:mm').format(dateTime);
    (data as Map)["endDate"]= time;
    print(time);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsetsDirectional.fromSTEB(15, 10, 15, 7),
      child: GestureDetector(
        onTap: () {

          Navigator.push(context,
              MaterialPageRoute(builder: (context) => BookingDetailsScreen(texts: widget.texts,)));
        },
        child: Card(

          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16),
            //set border radius more than 50% of height and width to make circle
          ),
          elevation: 7,
          color: Colors.white,
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            child: Column(
              children: [
                Text(
                  (widget.texts as Map)["status"]=='Approved'? 'Status: Approved' : 'Status: Declined',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                Divider(),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text(
                                (data as Map)["startDate"],
                                style: TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                (widget.texts as Map)["departureCode"],
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                          Flexible(
                            child: Image.asset(
                              'lib/assets/images/bookings.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                          Column(
                            children: [
                              Text(
                                (data as Map)["endDate"],
                                style: TextStyle(
                                    fontSize: 22, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                (widget.texts as Map)["destinationCode"],
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            (widget.texts as Map)["departureName"],
                            style: TextStyle(
                              fontSize: 12,
                              color: _greyColor,
                            ),
                          ),
                          Text(
                            (widget.texts as Map)["destinationName"],
                            textAlign: TextAlign.end,
                            style: TextStyle(
                              fontSize: 12,
                              color: _greyColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Date',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: _greyColor,
                                ),
                              ),
                              Row(
                                children: [
                                  Icon(Icons.calendar_month_outlined),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    (data as Map)["date"],
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Seats',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: _greyColor,
                                ),
                              ),
                              Text(
                                (widget.texts as Map)["bookedSeats"].toString(),
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Flight',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: _greyColor,
                                ),
                              ),
                              Text(
                                'FL' + (widget.texts as Map)["flightId"].toString(),
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
