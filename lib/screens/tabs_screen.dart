import 'package:flutter/material.dart';
import 'package:senior_project/screens/profile_screens/profile_edit.dart';
import 'package:senior_project/screens/screens_from_tabs/inbox_screen.dart';
import 'package:senior_project/screens/screens_from_tabs/profile_screen.dart';
import 'package:senior_project/screens/widgets/sidebar.dart';
import 'package:go_router/go_router.dart';
import 'screens_from_tabs/my_bookings_screens/my_bookings_screen.dart';
import 'screens_from_tabs/offers_screen.dart';
import 'screens_from_tabs/search_flight_screens/main_screen.dart';

class TabsScreen extends StatefulWidget {
  final int? startingIndex;

  const TabsScreen({super.key, this.startingIndex, Text points=const Text("")});

  @override
  State<TabsScreen> createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {


  final List<Widget> _pages = [
    MainScreen(),
    MyBookingsScreen(),
    ProfileScreen()
  ];

  final List<String> _widgetTitles = [
    "Book Flight",
    "My Bookings",
    "My Profile"
  ];
  int _selectedPageIndex = 0;

  @override
  void initState() {
    if (widget.startingIndex != null) {
      _selectedPageIndex = widget.startingIndex!;
    }
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;


    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromRGBO(249, 251, 250, 1),
      child: Scaffold(
        appBar: AppBar(

          backgroundColor: Color.fromRGBO(249, 251, 250, 1),
          leading: (_selectedPageIndex == 4)
              ? IconButton(
                  color: Colors.black,
                  icon: const Icon(Icons.edit_outlined),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => const ProfileEditPage(),
                      ),
                    );
                  },
                )
              : Container(),
          title: Center(
            child: Text(
              _widgetTitles[_selectedPageIndex],
              style: const TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold),
            ),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        endDrawer: const Drawer(
          child: SidebarMenu(),
        ),
        body: _pages[_selectedPageIndex],
        bottomNavigationBar: BottomNavigationBar(
          selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold),
          backgroundColor:  Color.fromRGBO(132, 182, 248, 1),
          selectedItemColor: Colors.white,
          unselectedItemColor: Color.fromRGBO(238, 245, 255, 1),
          currentIndex: _selectedPageIndex,
          onTap: _selectPage,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined),
              label: 'Home',
              backgroundColor: Color.fromRGBO(132, 182, 248, 1),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list_alt_outlined),
              label: 'My Bookings',
              backgroundColor: Color.fromRGBO(132, 182, 248, 1),
            ),

            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined),
              label: 'Profile',
              backgroundColor: Color.fromRGBO(132, 182, 248, 1),
            )
          ],
        ),
      ),
    );
  }
}
