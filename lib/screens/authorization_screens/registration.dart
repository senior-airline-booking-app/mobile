import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:senior_project/screens/authorization_screens/login.dart';
import 'package:senior_project/helper/api.dart';
import 'package:http/http.dart' as http;

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({super.key});

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _isObscure1 = true;
  bool _isObscure2 = true;
  double spaceBetweenInputs = 10.0;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passportController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();

  Uri regUrl = Uri.parse('http://192.168.0.129:8080/api/v1/users');

  // final String basicAuth = 'Basic ${base64Encode(utf8.encode('12@12.com:asd'))}';

  Future<void> reg() async {
    final String email = _emailController.text;
    final String password = _passwordController.text;
    final String passportId = _passportController.text;
    final String name = _nameController.text;
    final String surname = _surnameController.text;
    var data = {
      'name': name,
      'surname': surname,
      'email': email,
      'password': password,
      'passportId': passportId
    };
    print('$email $password $passportId $name $surname');

    try {
      final response = await http.post(Uri.parse('$baseUrl/users'),
          headers: {
            'Content-Type': 'application/json',
            // 'Authorization': basicAuth
          },
          body: jsonEncode(data));
      if (response.statusCode == 200) {
        print(response.body);
        //
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const LoginPage()),
        );
      }
    } catch (e) {
      print(e);
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Reg unsuccessful. Please try again.'),
          duration: Duration(seconds: 3),
          behavior: SnackBarBehavior.floating,
          margin: EdgeInsets.only(bottom: 50),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0),
              topRight: Radius.circular(10.0),
            ),
          ),
        ),
      );
    }
  }

  void _togglePassword1() {
    setState(() {
      _isObscure1 = !_isObscure1;
    });
  }

  void _togglePassword2() {
    setState(() {
      _isObscure2 = !_isObscure2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
          elevation: 0,
          title: const Text(
            'Registration',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        body: Column(children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(32.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      TextFormField(
                        controller: _nameController,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.person_outline),
                          labelText: 'Name',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your name here',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: spaceBetweenInputs),
                      TextFormField(
                        controller: _surnameController,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.person_outline),
                          labelText: 'Surname',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your surname here',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: spaceBetweenInputs),
                      TextFormField(
                        controller: _emailController,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.email_outlined),
                          labelText: 'Email',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your email here',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: spaceBetweenInputs),
                      TextFormField(
                        controller: _passportController,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(FontAwesomeIcons.passport),
                          prefixIconColor: Colors.blueAccent,
                          labelText: 'Passport ID',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your passport ID here',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                      ),
                      SizedBox(height: spaceBetweenInputs),
                      TextFormField(
                        controller: _passwordController,
                        obscureText: _isObscure1,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.lock_outline),
                          labelText: 'Create password',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your password here',
                          suffixIcon: IconButton(
                              icon: Icon(_isObscure1
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                _togglePassword1();
                              }),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                        // onSaved: (val) => _password = val!,
                      ),
                      SizedBox(height: spaceBetweenInputs),
                      TextFormField(
                        obscureText: _isObscure2,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.lock_outline),
                          labelText: 'Repeat password',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your password again',
                          suffixIcon: IconButton(
                              icon: Icon(_isObscure2
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                _togglePassword2();
                              }),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                        // onSaved: (val) => _password = val!,
                      ),
                      const Padding(
                          padding: EdgeInsets.symmetric(vertical: 16.0),
                          child: Text(
                            'Password must contain a minimum of 8 characters',
                            style: TextStyle(
                                color: CupertinoColors.systemGrey2,
                                fontSize: 12),
                          )),
                      const Padding(
                          padding: EdgeInsets.symmetric(),
                          child: Text(
                            'Password must contain at least one special symbol',
                            style: TextStyle(
                                color: CupertinoColors.systemGrey2,
                                fontSize: 12),
                          )),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32),
              child: Column(children: <Widget>[


                Container(
                  height: 60,
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateColor.resolveWith(
                              (states) =>
                              Color.fromRGBO(132, 182, 248, 1)),
                      shape: MaterialStateProperty.all<
                          RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          )),
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        // Process data.
                      }
                      reg();
                    },
                    child: const Text(
                      'Register',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Text("Already have an account?"),
                    TextButton(
                      // onPressed: () => context.go('/registration'),
                      onPressed: () {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginPage()),
                        );
                      },
                      child: const Text('Login'),
                    )
                  ],
                )
              ]))
        ]));
  }
}
