import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/authorization_screens/registration.dart';
import 'package:senior_project/screens/tabs_screen.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:senior_project/helper/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../store/user_store.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool _isObscure = true;

  void pushNotification(String text) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(text),
        duration: const Duration(seconds: 3),
        behavior: SnackBarBehavior.floating,
        margin: const EdgeInsets.only(bottom: 50),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0),
            topRight: Radius.circular(10.0),
          ),
        ),
      ),
    );
  }

  Future<void> login() async {
    final String username = _usernameController.text;
    final String password = _passwordController.text;
    print(username);
    final String basicAuth =
        'Basic ${base64Encode(utf8.encode('$username:$password'))}';

    if (username == '' || password == '') {
      return;
    }

    print('$username $password');

    try {
      final response = await http.get(
        Uri.parse('$baseUrl/login_success'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': basicAuth
        },
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        print(response.body);
        final userResponse = json.decode(response.body);
        final user = User(
          id: userResponse['id'],
          name: userResponse['name'],
          email: userResponse['email'],
          surname: userResponse['surname'],
          passportId: userResponse['passportId'],
        );
        Provider.of<UserStore>(context, listen: false).setUser(user);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const TabsScreen()),
        );
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('token', basicAuth);
      } else if (response.statusCode == 401) {
        pushNotification('Wrong password or username');
      } else {
        pushNotification('Something went wrong. Please contact Rizar.');
      }
    } catch (e) {
      print(e);
    }
  }

  // Toggles the password show status
  void _togglePassword() {
    setState(() {
      _isObscure = !_isObscure;
    });
  }

  @override
  Widget build(BuildContext context) {
    _usernameController.text = "admin@gmail.com";
    _passwordController.text = "asd";

    return Scaffold(
        // backgroundColor: Colors.amber,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
          elevation: 0,
          title: const Text(
            'Login',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      TextFormField(
                        controller: _usernameController,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.email_outlined),
                          labelText: 'Email',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your email here',
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 15),
                      TextFormField(
                        controller: _passwordController,
                        obscureText: _isObscure,
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          prefixIcon: const Icon(Icons.lock_outline),
                          labelText: 'Password',
                          floatingLabelAlignment: FloatingLabelAlignment.start,
                          floatingLabelBehavior: FloatingLabelBehavior.always,
                          hintText: 'Enter your password here',
                          suffixIcon: IconButton(
                              icon: Icon(_isObscure
                                  ? Icons.visibility
                                  : Icons.visibility_off),
                              onPressed: () {
                                _togglePassword();
                              }),
                        ),
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'required';
                          }
                          return null;
                        },
                        // onSaved: (val) => _password = val!,
                      ),
                    ],
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Column(children: <Widget>[

                      Container(
                        height: 60,
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateColor.resolveWith(
                                    (states) =>
                                    Color.fromRGBO(132, 182, 248, 1)),
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                )),
                          ),

                            onPressed: () {
                              // Validate will return true if the form is valid, or false if
                              // the form is invalid.

                              // Navigator.pushReplacement(
                              //   context,
                              //   MaterialPageRoute(
                              //       builder: (context) => const TabsScreen()),
                              // );

                              if (_formKey.currentState!.validate()) {
                                // Process data
                              }
                              // Navigator.pushReplacement(
                              //   context,
                              //   MaterialPageRoute(
                              //       builder: (context) => const TabsScreen()),
                              // );
                              login();

                              // context.go('/main');
                            },
                          child: const Text(
                            'Login',
                            style: TextStyle(
                                letterSpacing: 1,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          const Text("Don't have an account?"),
                          TextButton(
                            // onPressed: () => context.go('/registration'),
                            onPressed: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const RegistrationPage()),
                              );
                            },
                            child: const Text('Register'),
                          )
                        ],
                      )
                    ]))
              ]),
        ));
  }
}
