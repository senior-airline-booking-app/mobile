import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:senior_project/helper/api.dart';
import 'package:senior_project/screens/search_results/search_result_tile.dart';
import 'package:http/http.dart' as http;
import 'dart:math';

import '../creation_screens/creation_screen.dart';
// import 'package:senior_project/helper/api.dart';

const results = [
  {
    'flight_id': '33',
    'dep_time': '',
    'dest_time': '',
    'price': '',
    'dep_port': '',
    'dest_port': '',
    'duration': ''
  },
  {
    'flight_id': '3',
    'dep_time': '',
    'dest_time': '',
    'price': '',
    'dep_port': '',
    'dest_port': '',
    'duration': ''
  },
  {
    'flight_id': '3',
    'dep_time': '',
    'dest_time': '',
    'price': '',
    'dep_port': '',
    'dest_port': '',
    'duration': ''
  }
];

class SearchResults extends StatefulWidget {
  dynamic data;

  SearchResults({Key? key, required this.data}) : super(key: key);

  @override
  State<SearchResults> createState() => _SearchResultsState();
}

class _SearchResultsState extends State<SearchResults> {
  dynamic _results = [];
  String? _token;

  fetchToken() async {
    await getAuth().then((value) {
      print('value in get auth $value');
      _token = value;
    });
  }

  // from, to, date
  dynamic _data;

  fetchData() async {
    await fetchToken();

    print(_data['departurePort']['id']);
    final Uri url = Uri.parse("$baseUrl/itineraries");
    // print(url.replace(queryParameters: {
    //   'departurePortId': _data['departurePort'],
    //   'destinationPortId': _data['destinationPort'],
    //   'expectedDate': _data['date'],
    // }));
    var response = await http.get(
      url.replace(queryParameters: {
        'departurePortId': _data['departurePort']['id'].toString(),
        'destinationPortId': _data['destinationPort']['id'].toString(),
        'expectedDate': _data['date'],
      }),
      headers: {'Content-Type': 'application/json', 'Authorization': _token!},
    );
    print('response ${response.body}');
    if (response.statusCode != 200) return;
    setState(() {
      _results = json.decode(response.body);
      print(_results);
    });
  }

  @override
  void initState() {
    fetchData();
    _data = widget.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(color: Colors.grey),
        backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
        title: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              'Search Results',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: _token != null
              ? (_results.length > 0 ? ListView.builder(
                  itemCount: _results.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8),
                      child: SearchResultTile(
                        details: _results[index],
                      ),
                    );
                  },
                ) :  Center(

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('There are no flights on this date.', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),),
                      TextButton(onPressed: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CreationScreen()),
                        );
                      }, child: Text('Create a new flight'))
                    ],
                  ),
                ))
              : const Text(
                  'Loading...',
                  style: TextStyle(
                      color: Colors.black, fontSize: 20),
                ),
        ),
      ),
    );
  }
}
