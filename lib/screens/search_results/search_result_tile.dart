import 'package:flutter/material.dart';
import 'package:senior_project/screens/passenger_info_screens/passenger_info_main.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/creation_screens/flight_details.dart';
import '../../store/flight_store.dart';

class SearchResultTile extends StatefulWidget {
  final dynamic details;

  const SearchResultTile({Key? key, required this.details}) : super(key: key);

  @override
  State<SearchResultTile> createState() => _SearchResultTileState();
}

class _SearchResultTileState extends State<SearchResultTile> {
  late dynamic _details;

  List<String> texts = [
    'Nur-Sultan International Airport',
    'Almaty International Airport'
  ];

  final Color _greyColor = const Color.fromRGBO(85, 85, 85, 1);

  void redirectToNext() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                const FlightDetailsScreen(searchScreen: true)));
  }

  void _setFlight(BuildContext context) {
    final flightStore = Provider.of<FlightStore>(context, listen: false);
    // final flight = {
    //   'flightNumber': 'XY123',
    //   'departure': 'New York',
    //   'destination': 'London',
    //   'departureTime': '2023-04-01T08:00:00Z',
    // };
    print('-------details-------');
    print(_details);
    flightStore.setFlight(_details);
  }

  @override
  void initState() {
    _details = widget.details;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsetsDirectional.fromSTEB(0, 0, 0, 0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        elevation: 7,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          child: Column(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'flight #${_details['flightId']}',
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                        Text(
                          '${_details['duration']}',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 12,
                            color: _greyColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${_details['departureTime'].split('T')[1].substring(0, 5)}',
                              style: const TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${_details['departureName'].toString().characters.take(8).toString()}(${_details['departureCode']})',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w600),
                            ),
                          ],
                        ),
                        Image.asset('lib/assets/images/bookings.png',
                            width: 100),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '${_details['destinationTime'].split('T')[1].substring(0, 5)}',
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '${_details['destinationName'].toString().characters.take(8).toString()}(${_details['destinationCode']})',
                              style: TextStyle(
                                  fontSize: 12, fontWeight: FontWeight.w600),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Available Seats ',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: _greyColor,
                              ),
                            ),
                            Text(
                              '${_details['availableSeats']}/8',
                              textAlign: TextAlign.end,
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'From ',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: _greyColor,
                              ),
                            ),
                            const Text(
                              '\$125',
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateColor.resolveWith(
                            (states) => Color.fromRGBO(132, 182, 248, 1)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        )),
                      ),
                      onPressed: () {
                        _setFlight(context);
                        redirectToNext();
                      },
                      child: const Text(
                        'Check',
                        style: TextStyle(
                            letterSpacing: 1,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
