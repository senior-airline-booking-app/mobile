import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/main_screen.dart';
import 'package:senior_project/screens/tabs_screen.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:dio/dio.dart';

class MapScreen extends StatefulWidget {
  const MapScreen(
      {Key? key,
      required this.destPositions,
      required this.deptPositions,
      required this.selecting,
      required this.ports})
      : super(key: key);
  final List<dynamic> ports;
  final String selecting;
  final Map<String, dynamic> destPositions;
  final Map<String, dynamic> deptPositions;
  @override
  State<MapScreen> createState() => MapScreenState();
}

class MapScreenState extends State<MapScreen> {
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();
  Dio _dio = Dio();
  CancelToken _cancelToken = CancelToken();
  static CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(23.432, 80.213),
    zoom: 14.4746,
  );
  List<Marker> markers = [];
  // Uint8List? marketimages;

  // Future<Uint8List> getImages(String path, int width) async {
  //   ByteData data = await rootBundle.load(path);
  //   ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
  //       targetHeight: width);
  //   ui.FrameInfo fi = await codec.getNextFrame();
  //   return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
  //       .buffer
  //       .asUint8List();
  // }

  @override
  void initState() {
    _destPositions = widget.destPositions;
    _deptPositions = widget.deptPositions;
    _markerDept = Marker(
      markerId: const MarkerId("departure"),
      position: LatLng(
          widget.deptPositions["latitude"], widget.deptPositions["longitude"]),
    );
    _markerDest = Marker(
      markerId: const MarkerId("destination"),
      position: LatLng(_destPositions["latitude"], _destPositions["longitude"]),
    );
    // TODO: implement initState
    super.initState();
    // initialize loadData method

    markers = widget.ports
        .map((port) => Marker(
            onTap: () {
              Navigator.pop(context, port);
            },
            infoWindow: InfoWindow(title: port["name"], snippet: ''),
            markerId: MarkerId(port["id"].toString()),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueBlue),
            position: LatLng(port["latitude"], port["longitude"])))
        .toList();
    // loadData();
    // _markerDept = Marker(
    //   markerId: const MarkerId("departure"),
    //   position: LatLng(
    //       widget.deptPositions["latitude"], widget.deptPositions["longitude"]),
    // );
    // _markerDest = Marker(
    //   markerId: const MarkerId("destination"),
    //   position: LatLng(_destPositions["latitude"], _destPositions["longitude"]),
    // );
    // }
    markers.add(_markerDept);
    markers.add(_markerDest);
    _kGooglePlex = CameraPosition(
      target: LatLng(widget.ports[0]["latitude"], widget.ports[0]["longitude"]),
      zoom: 14.4746,
    );
  }

  String blueMarker = 'lib/assets/images/blue_marker.png';

  // created method for displaying custom markers according to index
  // loadData() async {
  //   final Uint8List markIcons = await getImages(blueMarker, 100);
  //   // makers added according to index
  //   markers = widget.ports
  //       .map((port) => Marker(
  //           onTap: () {
  //             Navigator.pop(context, port);
  //           },
  //           // icon: BitmapDescriptor.fromBytes(markIcons),
  //           infoWindow: InfoWindow(title: port["name"], snippet: ''),
  //           markerId: MarkerId(port["id"].toString()),
  //           icon: BitmapDescriptor.defaultMarkerWithHue(
  //               BitmapDescriptor.hueGreen),
  //           position: LatLng(port["latitude"], port["longitude"])))
  //       .toList();
  //   _markerDept = Marker(
  //     markerId: const MarkerId("departure"),
  //     icon: BitmapDescriptor.fromBytes(markIcons),
  //     position: LatLng(
  //         widget.deptPositions["latitude"], widget.deptPositions["longitude"]),
  //   );
  //   _markerDest = Marker(
  //     markerId: const MarkerId("destination"),
  //     icon: BitmapDescriptor.fromBytes(markIcons),
  //     position: LatLng(_destPositions["latitude"], _destPositions["longitude"]),
  //   );
  //   setState(() {});
  // }

  @override
  var _destPositions;
  var _deptPositions;

  late Marker _markerDept;
  //  = Marker(
  //   markerId: MarkerId("departure"),
  //   icon: BitmapDescriptor.defaultMarker,
  //   position: LatLng(37.42796133580664, -122.085749655962),
  // );

  late Marker _markerDest;
  // =
  //  Marker(
  //   markerId: const MarkerId("destination"),
  //   icon: BitmapDescriptor.defaultMarker,
  //   position: LatLng(37.42796133580664, -122.085949655962),
  // );
  double calculateDistance(lat1, lon1, lat2, lon2) {
    // final position1 = await Geolocator.getCurrentPosition(
    //   desiredAccuracy: LocationAccuracy.high,
    //   latitude: lat1,
    //   longitude: lon1,
    // );

    Future<Uint8List> getBytesFromAsset(
        {required String path, required int width}) async {
      ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
          targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
          .buffer
          .asUint8List();
    }

    final distanceInMeters = Geolocator.distanceBetween(
      lat1,
      lon1,
      lat2,
      lon2,
    );

    return distanceInMeters / 1000; // return distance in kilometers
  }

  void waitingDialog(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible:
            false, // set to false if you want to prevent users from dismissing the dialog by tapping outside
        builder: (BuildContext context) {
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: 16.0),
                Text(
                  'Please wait...',
                  style: TextStyle(fontSize: 16.0),
                ),
                SizedBox(height: 16.0),
                CircularProgressIndicator(),
                TextButton(
                  child: const Text(
                    "Cancel",
                    style: TextStyle(fontSize: 14),
                  ),
                  onPressed: () {
                    _cancelToken.cancel('Request canceled');
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  Future<MapEntry<double, String>> getNearbyPlaces(
      LatLng center, BuildContext context) async {
    // String apiKey = "AIzaSyDCkTc2T6j2TI7qU2jHlJfbavLyYe9Qcsc";
    String apiKey = "AIzaSyAues8dw_usefVuVYKfmGAmPmBvPBqmCgY";
    Map<double, String> places = {};
    int radius = 5;
    var response;
    // var name = await createMarkerName(center);
    // print(' name $name');
    // waitingDialog(context);
    do {
      print("------------------radius");
      print(radius);
      String url =
          "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${center.latitude},${center.longitude}&radius=${radius}&key=${apiKey}";
      response = await http.get(Uri.parse(url));

      // try {
      //   response = await _dio.get(url, cancelToken: _cancelToken);
      //   // Process the response
      // } on DioError catch (e) {
      //   if (e.type == DioErrorType.cancel) {
      //     print('Request canceled');
      //   } else {
      //     print('Error: $e');
      //   }
      // }
      if (response.statusCode == 200) {
        print(response);
        var data = jsonDecode(response.body);
        if (data["status"] == "OK") {
          for (var place in data["results"]) {
            double lat = place["geometry"]["location"]["lat"];
            double lng = place["geometry"]["location"]["lng"];
            LatLng location = LatLng(lat, lng);
            List<Placemark> placemarks = await placemarkFromCoordinates(
                location.latitude, location.longitude);
            var placemark = placemarks.first;
            String name = placemark.name ?? '';
            String locality = placemark.locality ?? '';
            String thoroughfare = placemark.thoroughfare ?? '';
            String subThoroughfare = placemark.subThoroughfare ?? '';
            String address = '$name $thoroughfare $subThoroughfare, $locality';
            double distance =
                calculateDistance(lat, lng, center.latitude, center.longitude)
                    .roundToDouble();
            if (distance != 0.0) {
              places[distance] =
                  '${address.substring(1, 8)} ($distance km away)';
            } else {
              places[distance] = address.substring(1, 12);
            }
          }
        }
      }
      radius += 100;
      print(radius);
    } while (places.isEmpty);
    // print(places);
    List<MapEntry<double, String>> sortedPlaces = places.entries.toList()
      ..sort((a, b) => a.key.compareTo(b.key));
    return sortedPlaces.first;
  }

  // Future<String> createMarkerName(LatLng location) async {
  //   print("fucnt name");
  //   try {
  //     List<Placemark> placemarks =
  //         await placemarkFromCoordinates(location.latitude, location.longitude);
  //     if (placemarks != null && placemarks.isNotEmpty) {
  //       Placemark placemark = placemarks.first;
  //       String name = placemark.name ?? '';
  //       String locality = placemark.locality ?? '';
  //       String thoroughfare = placemark.thoroughfare ?? '';
  //       String subThoroughfare = placemark.subThoroughfare ?? '';
  //       String address = '$name $thoroughfare $subThoroughfare, $locality';
  //       return address;
  //     } else {
  //       return '';
  //     }
  //   } catch (e) {
  //     var nearby = await getNearbyPlaces(location);
  //     print('nearby $nearby');
  //     // return nearby[0];
  //     print(e);
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    // setState(() {
    //   markers = widget.ports
    //       .map((port) => Marker(
    //           onTap: () {
    //             Navigator.pop(context, port);
    //           },
    //           infoWindow: InfoWindow(title: port["name"], snippet: ''),
    //           markerId: MarkerId(port["id"].toString()),
    //           icon: BitmapDescriptor.defaultMarker,
    //           position: LatLng(port["latitude"], port["longitude"])))
    //       .toList();

    //   _markerDept = Marker(
    //     markerId: const MarkerId("departure"),
    //     position: LatLng(widget.deptPositions["latitude"],
    //         widget.deptPositions["longitude"]),
    //   );
    //   _markerDest = Marker(
    //     markerId: const MarkerId("destination"),
    //     position:
    //         LatLng(_destPositions["latitude"], _destPositions["longitude"]),
    //   );
    //   // }
    //   markers.add(_markerDept);
    //   markers.add(_markerDest);
    //   _kGooglePlex = CameraPosition(
    //     target:
    //         LatLng(widget.ports[0]["latitude"], widget.ports[0]["longitude"]),
    //     zoom: 14.4746,
    //   );
    // });
    // print(markers);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
        centerTitle: true,
        leading: const BackButton(color: Colors.grey),
        title: const Text(
          'Map',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
      body: GoogleMap(
        mapType: MapType.normal,
        markers: markers.toSet(), //{_markerDest, _markerDept},
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        onTap: (point) {
          setState(() {
            // print(markers);
            // print(widget.ports);
            // print("destposition" + _destPositions.toString());
            // print("deptposition" + _destPositions.toString());

            if (widget.selecting == "destination") {
              _markerDest =
                  Marker(markerId: MarkerId(widget.selecting), position: point);
              _destPositions.clear();
              _destPositions["latitude"] = point.latitude;
              _destPositions["longitude"] = point.longitude;
              markers.add(_markerDest);
            } else {
              _markerDept =
                  Marker(markerId: MarkerId(widget.selecting), position: point);
              widget.deptPositions.clear();
              setState(() {
                widget.deptPositions["latitude"] = point.latitude;
                widget.deptPositions["longitude"] = point.longitude;
              });
              markers.add(_markerDept);
            }
          });
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _Submit,
        label: const Text('Select'),
        icon: const Icon(Icons.check),
      ),
    );
  }

  Future<void> _Submit() async {
    // _destPositions["name"] = await createMarkerName(LatLng(
    //     _destPositions["latitude"], _destPositions["longitude"]));
    // ignore: avoid_print
    var nearby;
    waitingDialog(context);
    if (widget.selecting == "destination") {
      // name = await createMarkerName(LatLng(
      //     _destPositions["latitude"], _destPositions["longitude"]));
      nearby = await getNearbyPlaces(
          LatLng(_destPositions["latitude"], _destPositions["longitude"]),
          context);
      // ignore: avoid_print
      // print(nearby[0]);
      _destPositions["name"] = nearby.value;
    } else {
      nearby = await getNearbyPlaces(
          LatLng(_deptPositions["latitude"], _deptPositions["longitude"]),
          context);
      // nearby = await createMarkerName(LatLng(
      //     widget.deptPositions["latitude"], widget.deptPositions["longitude"]));
      // ignore: avoid_print
      setState(() {
        _deptPositions["name"] = nearby.value;
      });
    }
    print(nearby);
    Navigator.of(context).pop();
    // print(name);
    // ignore: avoid_print
    // print("nearby: $nearby");
    // // print(_destPositions);
    // print(widget.ports);
    // print("destposition last" + _destPositions.toString());
    // print("deptposition last" + _deptPositions.toString());
    // print("name: " + _destPositions["name"]);
    Navigator.pop(context,
        (widget.selecting == "destination") ? _destPositions : _deptPositions);
    // controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }
}
