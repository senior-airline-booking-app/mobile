import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/creation_screens/flight_details.dart';
import 'package:senior_project/screens/creation_screens/map_screen.dart';
import 'package:senior_project/screens/passenger_info_screens/passenger_info_main.dart';
import 'package:senior_project/screens/payment_screens/payment.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/date_picker_screen.dart';
import 'package:senior_project/screens/creation_screens/port_menu.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/time_picker_screen.dart';
import 'package:senior_project/screens/search_results/search_results_screen.dart';
import 'package:senior_project/screens/tabs_screen.dart';
import 'package:intl/intl.dart';
import 'package:senior_project/screens/widgets/recommendations_card.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:senior_project/helper/api.dart';
import 'dart:convert';
import '../../store/flight_store.dart';

class FlightDetailsScreen extends StatefulWidget {
  const FlightDetailsScreen({
    required this.searchScreen,
    super.key,
  });

  final bool searchScreen;

  @override
  State<FlightDetailsScreen> createState() => _FlightDetailsScreenState();
}

class _FlightDetailsScreenState extends State<FlightDetailsScreen> {
  var numPassengers = TextEditingController();
  final _form = GlobalKey<FormState>();
  var price = 0;

  // final _toFocusNode = FocusNode();
  final Color _greyColor = const Color.fromRGBO(85, 85, 85, 1);

  String? _token;

  fetchToken() async {
    await getAuth().then((value) {
      print('value in get auth $value');
      _token = value;
    });
    calculateEndTime();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchToken();
    numPassengers.text = "1";
    price = (widget.searchScreen) ? 125 : 1000;
  }

  calculateEndTime() async {
    final flightStore = Provider.of<FlightStore>(context, listen: false);
    final flight = flightStore.flight;

    print('flight in end time $flight');

    if (flight!['id'] == '-1') {
      final Uri urlTime = Uri.parse("$baseUrl/itineraries/destination-time");
      var params = {
        "depX": flight!['deptCoordinates'] == null
            ? flight!['departure']['latitude'].toString()
            : flight!['deptCoordinates']['latitude'].toString(),
        "depY": flight!['deptCoordinates'] == null
            ? flight!['departure']['longitude'].toString()
            : flight!['deptCoordinates']['longitude'].toString(),
        "destX": flight!['destCoordinates'] == null
            ? flight!['destination']['latitude'].toString()
            : flight!['destCoordinates']['latitude'].toString(),
        "destY": flight!['destCoordinates'] == null
            ? flight!['destination']['longitude'].toString()
            : flight!['destCoordinates']['longitude'].toString(),
        'departDate': flight!['departureTime'].toString().replaceAll('T', ' '),
      };
      var responseTime = await http.get(
        urlTime.replace(queryParameters: params),
        headers: {'Authorization': _token!, 'Content-Type': 'application/json'},
      );
      setState(() {
        flight['destinationTime'] = jsonDecode(responseTime.body);
      });

      print('query ${params}');
      print('responseTime ${responseTime.body.replaceAll('"', "")}');
    }
  }

  final Color _mainColor = Color.fromRGBO(132, 182, 248, 1);
  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        backgroundColor: Color.fromRGBO(38, 38, 38, 0.4),
        content: const Text("Maximum number of passengers is exceeded"),
        action: SnackBarAction(
            label: 'OK', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final flightStore = Provider.of<FlightStore>(context);
    final flight = flightStore.flight;
    if (!widget.searchScreen) numPassengers.text = flight?["passengerNum"];
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
          centerTitle: true,
          leading: const BackButton(color: Colors.grey),
          title: const Text(
            'Flight details',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          iconTheme: const IconThemeData(color: Colors.black),
        ),
        backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
        body: flight == null
            ? Center(child: Text('No flight details available'))
            : CustomScrollView(scrollDirection: Axis.vertical, slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Center(
                        child: Container(
                            margin: const EdgeInsetsDirectional.fromSTEB(
                                0, 0, 0, 0),
                            child: Form(
                                key: _form,
                                child: Column(children: [
                                  Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                    elevation: 7,
                                    color: Colors.white,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 15.0, vertical: 10.0),
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 12.0, bottom: 15.0),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          '${flight['departureTime'].split('T')[1].substring(0, 5)}',
                                                          style: const TextStyle(
                                                              fontSize: 22,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        Text(
                                                          // 'dep code',
                                                          flight!['deptCoordinates'] ==
                                                                  null
                                                              ? '${flight['departureCode']}'
                                                              : flight['deptCoordinates']
                                                                      ['name']
                                                                  .toString()
                                                                  .substring(
                                                                      0, 3)
                                                                  .toUpperCase(),
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ],
                                                    ),
                                                    Flexible(
                                                        child: Image.asset(
                                                            'lib/assets/images/bookings.png')),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      children: [
                                                        Text(
                                                          // 'desti',
                                                          '${flight['destinationTime'].split('T')[1].substring(0, 5)}',
                                                          style: TextStyle(
                                                              fontSize: 22,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                        Text(
                                                          // 'dest code',
                                                          flight!['destCoordinates'] ==
                                                                  null
                                                              ? '${flight['destinationCode']}'
                                                              : flight['destCoordinates']
                                                                      ['name']
                                                                  .toString()
                                                                  .substring(
                                                                      0, 3)
                                                                  .toUpperCase(),
                                                          // '${_details['destinationName']}(${_details['destinationCode']})',
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Divider(
                                                color: _greyColor,
                                              ),
                                              const SizedBox(height: 15.0),
                                              Row(
                                                children: [
                                                  Flexible(
                                                    child: TextFormField(
                                                      readOnly: true,
                                                      // onTap: () async {
                                                      //   final _selectedDate2 = await Navigator.push(
                                                      //     context,
                                                      //     MaterialPageRoute(
                                                      //         builder: (context) =>
                                                      //             const DatePickerScreen()),
                                                      //   );
                                                      //   setState(() {
                                                      //     // _selectedDate = _selectedDate2;
                                                      //     if (!mounted) return;
                                                      //   });
                                                      // },
                                                      style: const TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          fontSize: 16),
                                                      decoration:
                                                          InputDecoration(
                                                        prefixIcon: const Icon(Icons
                                                            .date_range_outlined),
                                                        labelText: 'Date',
                                                        hintText: DateFormat(
                                                                'MMMM d')
                                                            .format(DateTime
                                                                .parse(flight[
                                                                        'departureTime']
                                                                    .split(
                                                                        'T')[0])),
                                                        hintStyle:
                                                            const TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                                fontSize: 16),
                                                        focusedBorder:
                                                            OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color:
                                                                  _mainColor),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.0),
                                                        ),
                                                        border:
                                                            OutlineInputBorder(
                                                          borderSide: BorderSide(
                                                              color:
                                                                  _mainColor),
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      8.0),
                                                        ),
                                                        floatingLabelAlignment:
                                                            FloatingLabelAlignment
                                                                .start,
                                                        floatingLabelBehavior:
                                                            FloatingLabelBehavior
                                                                .always,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(height: 20),
                                              TextFormField(
                                                inputFormatters: [
                                                  FilteringTextInputFormatter
                                                      .allow(RegExp(r'[0-9]+'))
                                                ],
                                                readOnly: widget.searchScreen
                                                    ? false
                                                    : true,
                                                controller: numPassengers,
                                                onChanged: (value) => {
                                                  setState(() {
                                                    // print(flight[
                                                    //     'availableSeats']);

                                                    if (value != "") {
                                                      if (int.parse(value) >
                                                          flight[
                                                              'availableSeats']) {
                                                        _showToast(context);
                                                        numPassengers
                                                            .text = flight[
                                                                'availableSeats']
                                                            .toString();
                                                      }
                                                      price = int.parse(value) *
                                                          125;
                                                      if (price >
                                                          flight['availableSeats'] *
                                                              125) {
                                                        price = flight[
                                                                'availableSeats'] *
                                                            125;
                                                      }
                                                    }
                                                    print(price);
                                                  })
                                                },
                                                keyboardType:
                                                    TextInputType.number,
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: 16),
                                                decoration: InputDecoration(
                                                  prefixIcon:
                                                      const Icon(Icons.people),
                                                  labelText: 'Passengers',
                                                  hintStyle: const TextStyle(
                                                      color: Colors.black,
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 16),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: _mainColor),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                  ),
                                                  border: OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                        color: _mainColor),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8.0),
                                                  ),
                                                  floatingLabelAlignment:
                                                      FloatingLabelAlignment
                                                          .start,
                                                  floatingLabelBehavior:
                                                      FloatingLabelBehavior
                                                          .always,
                                                ),
                                                validator: (value) {
                                                  // if (value == null) {
                                                  //   return 'Please enter number of passengers';
                                                  // }
                                                  if (value == "") {
                                                    return 'Please enter number of passengers';
                                                  }
                                                  if (int.parse(value!) < 1 ||
                                                      int.parse(value) >
                                                          flight[
                                                              'availableSeats']) {
                                                    return 'The maximum number of passengers is ${flight['availableSeats']}';
                                                  }
                                                  return null;
                                                },
                                              ),
                                              SizedBox(height: 15),
                                              Padding(
                                                  padding:
                                                      EdgeInsets.symmetric(),
                                                  child: Text(
                                                    'Maximum number of passangers for this flight is ${flight['availableSeats']}',
                                                    style:
                                                        TextStyle(fontSize: 12),
                                                  )),
                                              const SizedBox(height: 15),
                                              Divider(
                                                color: _greyColor,
                                              ),
                                              Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 20.0),
                                                  child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text('Price: ',
                                                            style: TextStyle(
                                                              fontSize: 18,
                                                            )),
                                                        Text(
                                                          '\$${price}',
                                                          style: TextStyle(
                                                            fontSize: 30,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        )
                                                      ])),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  SizedBox(
                                                    width: 100.0,
                                                    child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                              flight!['deptCoordinates'] ==
                                                                      null
                                                                  ? '${flight['departureName']}'
                                                                  : flight['deptCoordinates']['name'],
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                              style: TextStyle(
                                                                fontSize: 15,
                                                                color:
                                                                    _greyColor,
                                                              )),
                                                          // Text(
                                                          //   'code',
                                                          //   // '${flight['destination']}',
                                                          //   // '${_details['availableSeats']}/8',
                                                          //   textAlign:
                                                          //       TextAlign.end,
                                                          //   style:
                                                          //       const TextStyle(
                                                          //     fontSize: 16,
                                                          //   ),
                                                          // ),
                                                        ]),
                                                  ),
                                                  SizedBox(
                                                      width: 100.0,
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Text(
                                                              flight!['destCoordinates'] ==
                                                                  null
                                                                  ? '${flight['destinationName']}'
                                                                  : flight['destCoordinates']['name'],
                                                              textAlign:
                                                                  TextAlign.end,
                                                              style:
                                                                  const TextStyle(
                                                                fontSize: 16,
                                                              ),
                                                            ),
                                                            // Text(
                                                            //   'code',
                                                            //   // '${flight['destination']}',
                                                            //   // '${_details['availableSeats']}/8',
                                                            //   textAlign:
                                                            //       TextAlign.end,
                                                            //   style:
                                                            //       const TextStyle(
                                                            //     fontSize: 16,
                                                            //   ),
                                                            // ),
                                                          ]))
                                                ],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Flexible(
                                          child: SizedBox(
                                        width: double.infinity,
                                        child: ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                                  MaterialStateColor
                                                      .resolveWith((states) =>
                                                          const Color.fromRGBO(
                                                              255,
                                                              255,
                                                              255,
                                                              1)),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              ),
                                              side: const BorderSide(
                                                  width: 1,
                                                  color: Color.fromRGBO(
                                                      132, 182, 248, 1)),
                                              elevation: 0),
                                          onPressed: () {
                                            Navigator.pop(context);
                                            // _setFlight(context);
                                            // redirectToNext();
                                          },
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 18.0),
                                            child: Text(
                                              'Cancel',
                                              style: TextStyle(
                                                  color: _mainColor,
                                                  letterSpacing: 1,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      )),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Flexible(
                                        child: SizedBox(
                                          width: double.infinity,
                                          child: ElevatedButton(
                                            style: ButtonStyle(
                                              backgroundColor:
                                                  MaterialStateColor
                                                      .resolveWith((states) =>
                                                          Color.fromRGBO(132,
                                                              182, 248, 1)),
                                              shape: MaterialStateProperty.all<
                                                      RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              )),
                                            ),
                                            onPressed: () {
                                              final isValid = _form.currentState
                                                  ?.validate();
                                              if (!isValid!) return;
                                              _form.currentState?.save();

                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        PassengerInfoMain(
                                                            num: int.parse(
                                                                numPassengers
                                                                    .text))),
                                              );
                                              // _setFlight(context);
                                              // redirectToNext();
                                            },
                                            //   child: Padding(
                                            //   padding: EdgeInsets.symmetric(vertical: 18.0),
                                            //   child: Text(
                                            //     'Cancel',
                                            //     style: TextStyle(
                                            //         color: _mainColor,
                                            //         letterSpacing: 1,
                                            //         fontSize: 18,
                                            //         fontWeight: FontWeight.bold),
                                            //   ),
                                            // ),
                                            child: const Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 18.0),
                                              child: Text(
                                                'Proceed',
                                                style: TextStyle(
                                                    letterSpacing: 1,
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ])))),
                  ),
                )
              ]));
  }
}
