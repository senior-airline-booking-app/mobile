import 'package:flutter/material.dart';
import 'package:senior_project/screens/creation_screens/map_screen.dart';

class PortMenuScreen extends StatefulWidget {
  final Function(dynamic definer, dynamic port)? setPort;

  PortMenuScreen(
      {Key? key,
      required this.direction,
      required this.ports,
      required this.creating,
      this.setPort})
      : super(key: key);
  String direction;
  bool creating;
  List<dynamic> ports;
  @override
  State<PortMenuScreen> createState() => _PortMenuScreenState();
}

class _PortMenuScreenState extends State<PortMenuScreen> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      margin: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 0),
      // constraints: BoxConstraints(maxHeight: height * 0.5, minHeight: 10.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.direction,
            textAlign: TextAlign.start,
            style: const TextStyle(
                fontSize: 25, color: Colors.black, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 16.0),
          TextField(
            decoration: InputDecoration(
              filled: true,
              fillColor: const Color(0xF4F4F4F4),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.circular(15),
              ),
              hintText: 'Search',
              prefixIcon: const Icon(Icons.search),
              suffixIcon: Container(
                child: Container(
                  width: 60,
                  height: 25.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          // width
                          onPressed: () {
                            Navigator.pop(context, {"name": "", "code": ""});
                          },
                          icon: const Icon(Icons.close)),
                      const SizedBox(width: 7)
                    ],
                  ),
                ),
              ),
            ),
          ),
          SingleChildScrollView(
              child: Container(
            constraints:
                BoxConstraints(maxHeight: height * 0.6, minHeight: 10.0),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.ports.length,
              // physics: AlwaysScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                    contentPadding: const EdgeInsets.only(top: 7.0),
                    title: Column(children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                              child: Text((widget.ports[index] as Map)['name'],
                                  style: const TextStyle(fontSize: 18.0))),
                          Text((widget.ports[index] as Map)['code'],
                              style: const TextStyle(fontSize: 20.0)),
                        ],
                      ),
                      const SizedBox(height: 5.0),
                      Divider(color: Colors.grey.shade300),
                    ]),
                    onTap: () {
                      if (widget.setPort != null)
                        widget.setPort!(widget.direction, widget.ports[index]);
                      Navigator.pop(
                          context,
                          (widget.ports[index]
                              as Map)); //ports[index]['name']);
                    });
              },
            ),
          ))

          // Text("TExt"),
        ],
      ),
    );
  }
}
