import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/creation_screens/flight_details.dart';
import 'package:senior_project/screens/creation_screens/map_screen.dart';
import 'package:senior_project/screens/passenger_info_screens/passenger_info_main.dart';
import 'package:senior_project/screens/payment_screens/payment.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/date_picker_screen.dart';
import 'package:senior_project/screens/creation_screens/port_menu.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/time_picker_screen.dart';
import 'package:senior_project/screens/search_results/search_results_screen.dart';
import 'package:senior_project/screens/tabs_screen.dart';
import 'package:intl/intl.dart';
import 'package:senior_project/screens/widgets/recommendations_card.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:senior_project/helper/api.dart';
import 'dart:convert';

import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

import '../../store/flight_store.dart';

class CreationScreen extends StatefulWidget {
  const CreationScreen({super.key});

  @override
  State<CreationScreen> createState() => _CreationScreenState();
}

class _CreationScreenState extends State<CreationScreen> {
  final _form = GlobalKey<FormState>();
  Color _mainColor = Color.fromRGBO(132, 182, 248, 1);
  DateTime _selectedDate = DateTime.now().add(Duration(days: 1));
  DateTime _selectedTime = DateTime.now();
  DateTime _selectedTime2 = DateTime.now();
  var _data = {'from': '', 'to': '', 'time': ''};
  var numPassengers = TextEditingController();

  // numPassengers.text="1"
  // points = 0;

  var _destination = TextEditingController();
  var _departure = TextEditingController();
  var _deptPort;
  var _destPort;
  var destPortFull = {};
  var deptPortFull = {};
  Map<String, dynamic> _destPositions = {
    "name": "nothing",
    "longitude": 0.0,
    "latitude": 0.0
  };
  Map<String, dynamic> _deptPositions = {
    "name": "nothing",
    "longitude": 0.0,
    "latitude": 0.0
  };
  final _toFocusNode = FocusNode();
  var searchResults;
  bool isShared = false;

  void dispose() {
    _toFocusNode.dispose();
    numPassengers.dispose();
    _destination.dispose();
    _departure.dispose();
    super.dispose();
  }

  List<dynamic> ports = [
    {
      "id": 1,
      "code": "PMZ",
      "name": "Pomezia",
      "longitude": 12.4423,
      "latitude": 41.640215
    },
    {
      "id": 2,
      "code": "SNF",
      "name": "Spiaggia Naturista di Fiumicino",
      "longitude": 12.222426,
      "latitude": 41.786859
    }
  ];

  Future<void> getPorts() async {
    final String basicAuth =
        'Basic ${base64Encode(utf8.encode('admin@gmail.com:asd'))}';

    try {
      final response = await http.get(
        Uri.parse('$baseUrl/ports'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': basicAuth
        },
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        // print(response.body);
        ports = json.decode(response.body);
        print(ports);
        // Navigator.pushReplacement(
        //   context,
        //   MaterialPageRoute(builder: (context) => const TabsScreen()),
        // );
        // final prefs = await SharedPreferences.getInstance();
        // prefs.setString('token', basicAuth);
      } else {
        // pushNotification('Something went wrong. Please contact Rizar.');
      }
    } catch (e) {
      print(e);
    }
  }

  searchFlights() async {
    // var date = "2023-02-15T07:33:09.307Z";
    //
    // print(tempDate);
    final isValid = _form.currentState?.validate();
    if (!isValid!) return;
    _form.currentState?.save();
    // DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(_selectedDate.replaceAll(('T'), ' ').replaceAll('Z', ''));

    _data['date'] = DateFormat('yyyy-MM-dd').format(_selectedDate);

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => SearchResults(data: _data)));
  }

  void _showModal(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        enableDrag: false,
        isScrollControlled: false,
        useRootNavigator: true,
        builder: (BuildContext context) {
          return TimePickerScreen(
              selectedTime: _selectedTime, selectedTime2: _selectedTime2);
        }).then((value) => setState(() {
          if (value != null) _selectedTime = value;
        }));
  }

  setPort(definer, port) {
    print('$definer, $port}');
    if (definer == 'From') {
      deptPortFull = port;
    } else {
      destPortFull = port;
    }
  }

  void _showPortMenu(BuildContext ctx, String point) {
    showModalBottomSheet(
        context: ctx,
        enableDrag: false,
        isScrollControlled: true,
        useRootNavigator: true,
        builder: (BuildContext context) {
          return PortMenuScreen(
              direction: point, ports: ports, creating: false);
        }).then((value) {
      if (value != null) {
        setState(() {
          if (value["name"] != "") {
            if (point == "From") {
              _deptPort = value;
              _departure.text = value["name"];
            } else {
              _destPort = value;
              _destination.text = value["name"];
            }
          }
        });
      }
    });
  }

  void _setFlight(BuildContext context) {
    final flightStore = Provider.of<FlightStore>(context, listen: false);
    // _deptPositions['name'] = _deptPositions['name'].toString().substring(0,12);
    // _destPositions['name'] = _destPositions['name'].toString().substring(0,12);
    // print(
    //     'diff ${_destPositions['name']} ${_destPositions['name'].toString().substring(0, 12)}');
    final flight = {
      'id': '-1',
      'flightNumber': '-1',
      'departureName': _deptPort?['name'],
      'departure': _deptPort,
      'destinationName': _destPort?['name'],
      'destination': _destPort,
      'departureCode': _deptPort?['code'],
      'destinationCode': _destPort?['code'],
      'departureTime':
          '${_selectedDate.toString().split(' ')[0]}T${_selectedTime.toString().split(' ')[1]}',
      'destinationTime':
          '${_selectedDate.toString().split(' ')[0]}T${_selectedTime.toString().split(' ')[1]}',
      'isShared': isShared,
      'passengerNum': numPassengers.text,
      'duration': '01hr 30min',
      'price': 1000,
      'availableSeats': 8,
      'deptCoordinates': _deptPositions['name'] == 'nothing'
          ? null
          : {
              ..._deptPositions,
              'name': _deptPositions['name'].toString().substring(0, 12)
            },
      'destCoordinates': _destPositions['name'] == 'nothing'
          ? null
          : {
              ..._destPositions,
              'name': _destPositions['name'].toString().substring(0, 12)
            },
    };
    print('----details----');
    print(flight);
    flightStore.setFlight(flight);
  }

  handleCreate(BuildContext context) {
    _setFlight(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getPorts();
    print(ports);

    numPassengers.text = '1';
  }

  @override
  Widget build(BuildContext context) {
    // double width = MediaQuery.of(context).size.width;
    // double height = MediaQuery.of(context).size.height;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
        centerTitle: true,
        leading: const BackButton(color: Colors.grey),
        title: const Text(
          'Create booking',
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        spreadRadius: 2,
                        blurRadius: 3,
                        offset: Offset(0, 5), // changes position of shadow
                      ),
                    ],
                  ),
                  margin: const EdgeInsets.all(15.0),
                  child: Card(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16.0, vertical: 16.0),
                      child: Column(
                        children: [
                          Form(
                            key: _form,
                            child: Column(
                              children: [
                                // Expanded(child:

                                TextFormField(
                                  readOnly: true,
                                  controller: _departure,
                                  onTap: () {
                                    _showPortMenu(context, "From");
                                  },
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.airplane_ticket_outlined),
                                    suffixIcon: IconButton(
                                        onPressed: () async {
                                          var d2 = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => MapScreen(
                                                    destPositions:
                                                        _destPositions,
                                                    deptPositions:
                                                        _deptPositions,
                                                    selecting: "departure",
                                                    ports: ports)),
                                          );
                                          print(d2);
                                          if (d2 != null &&
                                              (d2["latitude"] != 0.0 &&
                                                  d2["longitude"] != 0.0)) {
                                            setState(() {
                                              print("creation screen" +
                                                  d2.toString());
                                              _deptPositions = d2;
                                              _departure.text =
                                                  d2["name"].toString();
                                            });
                                          }
                                        },
                                        icon: Icon(Icons.location_on)),
                                    labelText: 'Departure',
                                    hintText: "From",
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.red),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context)
                                        .requestFocus(_toFocusNode);
                                  },
                                  onSaved: (val) {
                                    _data['from'] = val!;
                                  },
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter departure port';
                                    }
                                    return null;
                                  },
                                ),
                                const SizedBox(height: 15),
                                TextFormField(
                                  readOnly: true,
                                  controller: _destination,
                                  onTap: () {
                                    _showPortMenu(context, "To");
                                  },
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon:
                                        Icon(Icons.airplane_ticket_outlined),
                                    suffixIcon: IconButton(
                                        onPressed: () async {
                                          var d = await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => MapScreen(
                                                    destPositions:
                                                        _destPositions,
                                                    deptPositions:
                                                        _deptPositions,
                                                    selecting: "destination",
                                                    ports: ports)),
                                          );
                                          if (d != null &&
                                              (d["latitude"] != 0.0 &&
                                                  d["longitude"] != 0.0)) {
                                            setState(() {
                                              _destPositions = d;
                                              _destination.text =
                                                  d["name"].toString();
                                            });
                                          }
                                        },
                                        icon: Icon(Icons.location_on)),
                                    labelText: 'Destination',
                                    hintText: "To",
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.red),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  focusNode: _toFocusNode,
                                  onSaved: (val) {
                                    _data['to'] = val!;
                                  },
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter destination port';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Flexible(
                                      child: TextFormField(
                                        readOnly: true,
                                        onTap: () async {
                                          final _selectedDate2 =
                                              await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const DatePickerScreen()),
                                          );
                                          setState(() {
                                            _selectedDate = _selectedDate2;
                                            if (!mounted) return;
                                          });
                                        },
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16),
                                        decoration: InputDecoration(
                                          prefixIcon: const Icon(
                                              Icons.date_range_outlined),
                                          labelText: 'Departure Date',
                                          hintText: DateFormat('MMMM d')
                                              .format(_selectedDate),
                                          hintStyle: const TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: _mainColor),
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          border: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: _mainColor),
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          floatingLabelAlignment:
                                              FloatingLabelAlignment.start,
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Flexible(
                                      child: TextFormField(
                                        readOnly: true,
                                        onTap: () async {
                                          _showModal(context);
                                        },
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 16),
                                        decoration: InputDecoration(
                                          prefixIcon: const Icon(
                                              Icons.date_range_outlined),
                                          labelText: 'Time',
                                          hintText: DateFormat('hh:mm')
                                              .format(_selectedTime),
                                          hintStyle: const TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 16),
                                          focusedBorder: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: _mainColor),
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          border: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: _mainColor),
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          floatingLabelAlignment:
                                              FloatingLabelAlignment.start,
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.always,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                TextFormField(
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'[0-9]+'))
                                  ],
                                  controller: numPassengers,
                                  keyboardType: TextInputType.number,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.people),
                                    labelText: 'Number of passengers',
                                    hintStyle: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: _mainColor),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: _mainColor),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  validator: (value) {
                                    // if (value == null) {
                                    //   return 'Please enter number of passengers';
                                    // }
                                    if (value == "") {
                                      return 'Please enter number of passengers';
                                    }
                                    if (int.parse(value!) < 1 ||
                                        int.parse(value) > 8) {
                                      return 'The number of passengers can between 1 and 8';
                                    }
                                    return null;
                                  },
                                ),
                                Row(
                                  children: [
                                    Switch(
                                      onChanged: (val) {
                                        setState(() {
                                          isShared = val;
                                        });
                                      },
                                      value: isShared,
                                      activeColor: Colors.blue,
                                      activeTrackColor:
                                          Color.fromRGBO(132, 182, 248, 1),
                                      inactiveThumbColor: Colors.grey,
                                      inactiveTrackColor: Colors.grey,
                                    ),
                                    Text("Shared")
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Container(
                            height: 60,
                            width: double.infinity,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) =>
                                        Color.fromRGBO(132, 182, 248, 1)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                )),
                              ),
                              onPressed: () {
                                final isValid = _form.currentState?.validate();
                                if (!isValid!) return;
                                _form.currentState?.save();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const FlightDetailsScreen(
                                            searchScreen: false,
                                          )),
                                );
                                handleCreate(context);
                              },
                              child: const Text(
                                'Create a new flight',
                                style: TextStyle(
                                    letterSpacing: 1,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
