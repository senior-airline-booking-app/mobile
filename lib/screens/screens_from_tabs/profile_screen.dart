import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/store/user_store.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  double spaceBetweenInputs = 22.0;

  @override
  Widget build(BuildContext context) {
    final userStore = Provider.of<UserStore>(context);
    final user = userStore.user;

    var information = [
      InfoItem("Name", user!.name, const Icon(Icons.portrait)),
      InfoItem("Surname", user!.surname, const Icon(Icons.portrait)),
      InfoItem("Email address", user!.email,
          const Icon(Icons.email_outlined)),
      InfoItem("Passport ID", user!.passportId, const Icon(Icons.fingerprint))
    ];

    return Scaffold(
        body: user == null
            ? const Center(child: Text('No user found'))
            : Padding(
                padding: const EdgeInsets.all(32.0),
                child: Column(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          for (var info in information)
                            Container(
                                margin:
                                    EdgeInsets.only(top: spaceBetweenInputs),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      info.title,
                                      textAlign: TextAlign.start,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14,
                                          color:
                                              Color.fromRGBO(132, 182, 248, 1)),
                                    ),
                                    const SizedBox(
                                      height: 10, // <-- SEE HERE
                                    ),
                                    Text(
                                      info.placeholder,
                                      textAlign: TextAlign.start,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 16),
                                    ),
                                    const Divider(
                                        height: 10,
                                        thickness: 1,
                                        indent: 0,
                                        endIndent: 0,
                                        color:
                                            Color.fromRGBO(132, 182, 248, 0.3)),
                                  ],
                                )),
                        ],
                      ),
                    ]),
              ));
  }
}

class InfoItem {
  InfoItem(
      [this.title = '',
      this.placeholder = '',
      this.icon = const Icon(Icons.email)]);

  String title;
  String placeholder;
  Icon icon;
}
