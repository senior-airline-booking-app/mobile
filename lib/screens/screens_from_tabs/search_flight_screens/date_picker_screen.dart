import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paged_vertical_calendar/paged_vertical_calendar.dart';

class DatePickerScreen extends StatefulWidget {
  const DatePickerScreen({Key? key}) : super(key: key);

  @override
  State<DatePickerScreen> createState() => _DatePickerScreenState();
}

class _DatePickerScreenState extends State<DatePickerScreen> {
  DateTime _selectedDate = DateTime.now().add(Duration(days: 1));

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Color.fromRGBO(249, 251, 250, 1),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(249, 251, 250, 1),
          centerTitle: true,
          title: Text(
            'Departure Date',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: Column(
          children: [
            Expanded(
              child: PagedVerticalCalendar(
                minDate: DateTime.now(),
                maxDate: DateTime.now().add(Duration(days: 60)),
                initialDate: DateTime.now(),
                onDayPressed: (date) {
                  print(date);
                  setState(() {
                    _selectedDate = date;
                  });
                },

                monthBuilder: (context, month, year) {
                  return Column(
                    children: [
                      Container(
                        padding: EdgeInsetsDirectional.fromSTEB(12, 16, 0, 12),
                        alignment: Alignment.topLeft,
                        child: Row(
                          children: [
                            Text(
                              DateFormat('MMMM').format(DateTime(year, month)),
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(85, 85, 85, 1)),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              DateFormat('yyyy').format(DateTime(year, month)),
                              style: TextStyle(
                                  fontSize: 19,
                                  fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(85, 85, 85, 1)),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsetsDirectional.fromSTEB(12, 8, 12, 8),
                        margin: EdgeInsetsDirectional.fromSTEB(0, 0, 0, 10),
                        color: Color.fromRGBO(230, 232, 231, 0.5),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            weekText('Mon'),
                            weekText('Tue'),
                            weekText('Wed'),
                            weekText('Thu'),
                            weekText('Fri'),
                            weekText('Sat'),
                            weekText('Sun'),
                          ],
                        ),
                      ),
                    ],
                  );
                },
                dayBuilder: (context, date) {
                  final bool isDisabled = date.isBefore(DateTime.now());
                  return Column(
                    children: [
                      isDisabled? IgnorePointer(
                    ignoring: true, // set to true to disable the child widget
                    child:
                    Container(

                        width: 29,
                        height: 27,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(2),
                          color: Color.fromRGBO(230, 232, 231, 1)
                        ),
                        child: Center(
                          child: Text(
                            DateFormat('d').format(date),
                            style: TextStyle(
                              fontSize:
                              16,
                              color: Color.fromRGBO(85, 85, 85, 1),
                            ),
                          ),
                        )),
                  )
:
                  Container(

                          width: 29,
                          height: 27,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            color: _selectedDate == date
                                ? Color.fromRGBO(132, 182, 248, 1)
                                : isDisabled
                                ? Color.fromRGBO(230, 232, 231, 1)
                                : Color.fromRGBO(236, 243, 253, 1),
                          ),
                          child: Center(
                            child: Text(
                              DateFormat('d').format(date),
                              style: TextStyle(
                                fontSize:
                                16,
                                color: _selectedDate == date
                                    ? Colors.white
                                    : Color.fromRGBO(85, 85, 85, 1),
                              ),
                            ),
                          )),
                    ],
                  );
                },
              ),
            ),
            Card(
              elevation: 10,
              child: Container(
                color: Colors.white,
                margin: const EdgeInsets.symmetric(
                    horizontal: 32, vertical: 20),
                height: 60,
                width: double.infinity,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateColor.resolveWith(
                        (states) => Color.fromRGBO(132, 182, 248, 1)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    )),
                  ),
                  onPressed: () {
                    DateTime _nowDate = DateTime.now().add(Duration(days: 1));

                    if(_selectedDate.isBefore(_nowDate)){
                      _selectedDate = DateTime.now().add(Duration(days: 1));
                    }
                    Navigator.pop(context, _selectedDate);
                  },
                  child: const Text(
                    'Select',
                    style: TextStyle(
                        letterSpacing: 1,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}

Widget weekText(String text) {
  return Container(
    width: 36,
    child: Center(
      child: Text(
        text,
        style: TextStyle(
            color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold),
      ),
    ),
  );
}
