import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

class TimePickerScreen extends StatefulWidget {
  TimePickerScreen(
      {Key? key, required this.selectedTime, required this.selectedTime2})
      : super(key: key);
  DateTime selectedTime;
  DateTime selectedTime2;
  @override
  State<TimePickerScreen> createState() => _TimePickerScreenState();
}

class _TimePickerScreenState extends State<TimePickerScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
      height: 250,
      child: Column(
        children: [
          TimePickerSpinner(
            is24HourMode: false,
            itemHeight: 50,
            normalTextStyle: const TextStyle(
                color: Color.fromRGBO(174, 174, 174, 1), fontSize: 23),
            highlightedTextStyle: const TextStyle(
                color: Color.fromRGBO(22, 25, 28, 1), fontSize: 26),
            onTimeChange: (time) {
              widget.selectedTime2 = time;
            },
          ),
          Container(
            color: Colors.white,
            margin: const EdgeInsets.symmetric(horizontal: 32, vertical: 20),
            height: 60,
            width: double.infinity,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateColor.resolveWith(
                    (states) => const Color.fromRGBO(132, 182, 248, 1)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                )),
              ),
              onPressed: () {
                setState(() {
                  widget.selectedTime = widget.selectedTime2;
                });
                Navigator.pop(context, widget.selectedTime);
              },
              child: const Text(
                'Choose time',
                style: TextStyle(
                    letterSpacing: 1,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
      // )
    );
  }
}
