import 'package:flutter/material.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'package:intl/intl.dart';
import 'package:senior_project/screens/creation_screens/port_menu.dart';

import 'package:senior_project/screens/creation_screens/creation_screen.dart';
import 'package:senior_project/screens/creation_screens/map_screen.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/date_picker_screen.dart';
import 'package:senior_project/screens/creation_screens/port_menu.dart';
import 'package:senior_project/screens/search_results/search_results_screen.dart';
import 'package:senior_project/screens/widgets/recommendations_card.dart';
import 'package:http/http.dart' as http;
import 'package:senior_project/helper/api.dart';
import 'dart:convert';
import 'package:senior_project/screens/payment_screens/payment.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final _form = GlobalKey<FormState>();
  var _deptPort;
  var _destPort;
  Color _mainColor = Color.fromRGBO(132, 182, 248, 1);
  DateTime _selectedDate = DateTime.now().add(Duration(days: 1));
  DateTime _selectedTime = DateTime.now();
  DateTime _selectedTime2 = DateTime.now();
  dynamic _data = {};

  // points = 0;
  var _destination = TextEditingController();
  var _departure = TextEditingController();
  List<double> _destPositions = [0.0, 0.0];
  List<double> _deptPositions = [0.0, 0.0];

  final _toFocusNode = FocusNode();
  var searchResults;

  List<dynamic> ports = [
    {
      "id": 1,
      "code": "PMZ",
      "name": "Pomezia",
      "longitude": 12.4423,
      "latitude": 41.640215
    },
    {
      "id": 2,
      "code": "SNF",
      "name": "Spiaggia Naturista di Fiumicino",
      "longitude": 12.222426,
      "latitude": 41.786859
    }
  ];

  Future<void> getPorts() async {
    final String basicAuth =
        'Basic ${base64Encode(utf8.encode('admin@gmail.com:asd'))}';

    try {
      final response = await http.get(
        Uri.parse('$baseUrl/ports'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': basicAuth
        },
      );
      print(response.statusCode);
      if (response.statusCode == 200) {
        // print(response.body);
        ports = json.decode(response.body);
        // print(ports);
        // Navigator.pushReplacement(
        //   context,
        //   MaterialPageRoute(builder: (context) => const TabsScreen()),
        // );
        // final prefs = await SharedPreferences.getInstance();
        // prefs.setString('token', basicAuth);
      } else {
        // pushNotification('Something went wrong. Please contact Rizar.');
      }
    } catch (e) {
      print(e);
    }
  }

  searchFlights() async {
    // var date = "2023-02-15T07:33:09.307Z";
    //
    // print(tempDate);
    final isValid = _form.currentState?.validate();
    if (!isValid!) return;
    _form.currentState?.save();
    // DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(_selectedDate.replaceAll(('T'), ' ').replaceAll('Z', ''));

    _data['date'] = DateFormat('yyyy-MM-dd').format(_selectedDate);
    _data['departurePort'] = _deptPort;
    _data['destinationPort'] = _destPort;
    // print(_data['from']);
    // print(_data['to']);
    // print(_data['date']);

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => SearchResults(data: _data)));
  }

  void _showModal(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        enableDrag: false,
        builder: (BuildContext context) {
          return Container(
            margin: EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
            height: 250,
            child: Column(
              children: [
                TimePickerSpinner(
                  is24HourMode: false,
                  itemHeight: 50,
                  normalTextStyle: const TextStyle(
                      color: Color.fromRGBO(174, 174, 174, 1), fontSize: 23),
                  highlightedTextStyle: const TextStyle(
                      color: Color.fromRGBO(22, 25, 28, 1), fontSize: 26),
                  onTimeChange: (time) {
                    _selectedTime2 = time;
                  },
                ),
                Container(
                  color: Colors.white,
                  margin:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 20),
                  height: 60,
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateColor.resolveWith(
                          (states) => Color.fromRGBO(132, 182, 248, 1)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      )),
                    ),
                    onPressed: () {
                      setState(() {
                        _selectedTime = _selectedTime2;
                      });
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Choose time',
                      style: TextStyle(
                          letterSpacing: 1,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  void _showPortMenu(BuildContext ctx, String point) {
    showModalBottomSheet(
        context: ctx,
        enableDrag: false,
        isScrollControlled: true,
        useRootNavigator: true,
        builder: (BuildContext context) {
          return PortMenuScreen(
              direction: point, ports: ports, creating: false);
        }).then((value) {
      if (value != null) {
        setState(() {
          if (value["name"] != "") {
            if (point == "From") {
              _deptPort = value;
              _departure.text = value["name"];
            } else {
              _destPort = value;
              _destination.text = value["name"];
            }
          }
        });
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPorts();
    print("initState");
    print(ports);
  }

  void dispose() {
    _toFocusNode.dispose();
    _destination.dispose();
    _departure.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // getPorts();

    // double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color.fromRGBO(249, 251, 250, 1),
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        spreadRadius: 2,
                        blurRadius: 3,
                        offset: Offset(0, 5), // changes position of shadow
                      ),
                    ],
                  ),
                  margin: EdgeInsets.all(15.0),
                  child: Card(
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsetsDirectional.fromSTEB(16, 16, 16, 8),
                      child: Column(
                        children: [
                          Form(
                            key: _form,
                            child: Column(
                              children: [
                                // Expanded(child:
                                TextFormField(
                                  readOnly: true,
                                  onTap: () async {
                                    _showPortMenu(context, "From");
                                  },
                                  controller: _departure,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(
                                        Icons.airplane_ticket_outlined),
                                    // suffixIcon: IconButton(
                                    //     onPressed: () async {
                                    //       var d2 = await Navigator.push(
                                    //         context,
                                    //         MaterialPageRoute(
                                    //             builder: (context) => MapScreen(
                                    //                 destPositions:
                                    //                     _destPositions,
                                    //                 deptPositions:
                                    //                     _deptPositions,
                                    //                 selecting: "departure",
                                    //                 ports: ports)),
                                    //       );
                                    //       _deptPositions = d2;
                                    //       _departure.text = d2.toString();
                                    //       setState(() {});
                                    //     },
                                    //     icon: Icon(Icons.location_on)),
                                    labelText: 'Departure',
                                    hintText: "From",
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.red),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  textInputAction: TextInputAction.next,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context)
                                        .requestFocus(_toFocusNode);
                                  },
                                  onSaved: (val) {
                                    _data['from'] = val!;
                                  },
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter departure port';
                                    }
                                    return null;
                                  },
                                ),
                                SizedBox(height: 15),
                                TextFormField(
                                  readOnly: true,
                                  controller: _destination,
                                  onTap: () async {
                                    _showPortMenu(context, "To");
                                  },
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon:
                                        Icon(Icons.airplane_ticket_outlined),
                                    // suffixIcon: IconButton(
                                    //     onPressed: () async {
                                    //       var d = await Navigator.push(
                                    //         context,
                                    //         MaterialPageRoute(
                                    //             builder: (context) => MapScreen(
                                    //                 destPositions:
                                    //                     _destPositions,
                                    //                 deptPositions:
                                    //                     _deptPositions,
                                    //                 selecting: "destination",
                                    //                 ports: ports)),
                                    //       );
                                    //       _destPositions = d;
                                    //       _destination.text = d.toString();
                                    //       setState(() {});
                                    //     },
                                    //     icon: Icon(Icons.location_on)),
                                    labelText: 'Destination',
                                    hintText: "To",
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.red),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  focusNode: _toFocusNode,
                                  onSaved: (val) {
                                    _data['to'] = val!;
                                  },
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Please enter destination port';
                                    }
                                    return null;
                                  },
                                ),
                                // IconButton(
                                //     onPressed: () async {
                                //       var d = await Navigator.push(
                                //         context,
                                //         MaterialPageRoute(
                                //             builder: (context) =>
                                //                 MapScreen(
                                //                     destPositions:
                                //                         _destPositions,
                                //                     deptPositions:
                                //                         _deptPositions,
                                //                     selecting:
                                //                         "destination")),
                                //       );
                                //       _destPositions = d;
                                //       _destination.text = d.toString();
                                //       setState(() {});
                                //     },
                                //     icon: Icon(Icons.location_on))

                                SizedBox(
                                  height: 15,
                                ),
                                // TextFormField(
                                //   readOnly: true,
                                //   onTap: () async {
                                //     final _selectedDate2 = await Navigator.push(
                                //       context,
                                //       MaterialPageRoute(
                                //           builder: (context) =>
                                //               const DatePickerScreen()),
                                //     );
                                //     setState(() {
                                //       _selectedDate = _selectedDate2;
                                //       if (!mounted) return;
                                //     });
                                //   },
                                //   style: TextStyle(
                                //       fontWeight: FontWeight.w600,
                                //       fontSize: 16),
                                //   decoration: InputDecoration(
                                //     prefixIcon: Icon(Icons.date_range_outlined),
                                //     labelText: 'Departure Date',
                                //     hintText: DateFormat('MMMM d')
                                //         .format(_selectedDate),
                                //     hintStyle: TextStyle(
                                //         color: Colors.black,
                                //         fontWeight: FontWeight.w600,
                                //         fontSize: 16),
                                //     focusedBorder: OutlineInputBorder(
                                //       borderSide: BorderSide(color: _mainColor),
                                //       borderRadius: BorderRadius.circular(8.0),
                                //     ),
                                //     border: OutlineInputBorder(
                                //       borderSide: BorderSide(color: _mainColor),
                                //       borderRadius: BorderRadius.circular(8.0),
                                //     ),
                                //     floatingLabelAlignment:
                                //         FloatingLabelAlignment.start,
                                //     floatingLabelBehavior:
                                //         FloatingLabelBehavior.always,
                                //   ),
                                // ),
                                TextFormField(
                                  readOnly: true,
                                  onTap: () async {
                                    final _selectedDate2 = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const DatePickerScreen()),
                                    );
                                    setState(() {
                                      _selectedDate = _selectedDate2;
                                      if (!mounted) return;
                                    });
                                  },
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16),
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(Icons.date_range_outlined),
                                    labelText: 'Departure Date',
                                    hintText: DateFormat('MMMM d')
                                        .format(_selectedDate),
                                    hintStyle: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 16),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: _mainColor),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(color: _mainColor),
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    floatingLabelAlignment:
                                        FloatingLabelAlignment.start,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            height: 60,
                            width: double.infinity,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateColor.resolveWith(
                                    (states) =>
                                        Color.fromRGBO(132, 182, 248, 1)),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                )),
                              ),
                              onPressed: () {
                                searchFlights();
                              },
                              child: const Text(
                                'Search for flights',
                                style: TextStyle(
                                    letterSpacing: 1,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          TextButton(
                              child: Text('Create a new flight'),
                              onPressed: () {
                                print("pressed");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CreationScreen()),
                                );
                              }),
                        ],
                      ),
                    ),
                  ),
                ),
                // Container(
                //   margin: EdgeInsets.all(15),
                //   child: Column(
                //     children: [
                //       Divider(),
                //       Row(
                //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //         children: [
                //           Text(
                //             'Recommendations',
                //             style: TextStyle(
                //                 fontSize: 18, fontWeight: FontWeight.bold),
                //           ),
                //           TextButton(
                //             onPressed: () {},
                //             child: Text(
                //               'See all',
                //               style: TextStyle(
                //                   fontSize: 15, fontWeight: FontWeight.w400),
                //             ),
                //           )
                //         ],
                //       ),
                //       Container(
                //         height: 125,
                //         child: ListView(
                //           scrollDirection: Axis.horizontal,
                //           children: <Widget>[
                //             RecommendCard(
                //                 color: Color.fromRGBO(242, 235, 225, 1)),
                //             SizedBox(
                //               width: 30,
                //             ),
                //             RecommendCard(
                //                 color: Color.fromRGBO(230, 225, 242, 1)),
                //             SizedBox(
                //               width: 30,
                //             ),
                //             RecommendCard(
                //                 color: Color.fromRGBO(238, 245, 255, 1))
                //           ],
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
