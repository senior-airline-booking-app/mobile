import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:senior_project/screens/widgets/booking_card.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:senior_project/helper/api.dart';

class MyBookingsScreen extends StatefulWidget {
  const MyBookingsScreen({super.key});

  @override
  State<MyBookingsScreen> createState() => _MyBookingsScreenState();
}

class _MyBookingsScreenState extends State<MyBookingsScreen> {
  Color _greyColor = Color.fromRGBO(85, 85, 85, 1);
  var data;
  String? _token;

  fetchToken() async {
    await getAuth().then((value) {
      print('value in get auth $value');
      _token = value;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    await fetchToken();
    var response = await http.get(Uri.parse("$baseUrl/bookings"),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': _token!
      },);
    print(response.body);
    data = json.decode(response.body);
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: data != null ? (data.length > 0 ? ListView(
        children: data?.map((d) { return BookingCard(texts: d,);}).toList().cast<Widget>(),
      ) : Center(child: Text('You have not booked any flight yet.'))) :
      Center(child: CircularProgressIndicator()),
    );
  }
}
