import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BookingDetailsScreen extends StatefulWidget {
  final Object texts;
  const BookingDetailsScreen({Key? key, required this.texts}) : super(key: key);

  @override
  State<BookingDetailsScreen> createState() => _BookingDetailsScreenState();
}

class _BookingDetailsScreenState extends State<BookingDetailsScreen> {
  Color _greyColor = Color.fromRGBO(85, 85, 85, 1);
  Object data = {'startTime': '', 'endTime': '', 'date': ''};
  var passengerData = [];

  // [
  // InfoItem("Name", 'Aruzhan', const Icon(Icons.portrait)),
  // InfoItem("Surname", 'Aruzhan', const Icon(Icons.portrait)),
  // InfoItem("Passport ID", 'Aruzhan', const Icon(Icons.fingerprint))
  // ], [
  // InfoItem("Name", 'Arun', const Icon(Icons.portrait)),
  // InfoItem("Surname", 'Aruz', const Icon(Icons.portrait)),
  // InfoItem("Passport ID", 'Aruz', const Icon(Icons.fingerprint))
  // ], [
  // InfoItem("Name", 'AAAA', const Icon(Icons.portrait)),
  // InfoItem("Surname", 'Aruzhan', const Icon(Icons.portrait)),
  // InfoItem("Passport ID", 'Aruzhan', const Icon(Icons.fingerprint))
  // ]

  initState() {
    DateTime dateTime = DateTime.parse((widget.texts as Map)["startTime"]);
    String output = DateFormat('dd/MM/yyyy').format(dateTime);
    (data as Map)["date"] = output;
    String time = DateFormat('HH:mm').format(dateTime);
    (data as Map)["startDate"] = time;
    dateTime = DateTime.parse((widget.texts as Map)["endTime"]);
    time = DateFormat('HH:mm').format(dateTime);
    (data as Map)["endDate"] = time;
    print(time);
    for(var passenger in (widget.texts as Map)["passengers"]){
      var info_items = [
        InfoItem("Name", passenger['name'].toString(), const Icon(Icons.portrait)),
        InfoItem("Surname", passenger['surname'].toString(), const Icon(Icons.portrait)),
        InfoItem("Passport ID", passenger['passportId'].toString(), const Icon(Icons.fingerprint))
      ];
      passengerData.add(info_items);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(249, 251, 250, 1),
        centerTitle: true,
        title: Text(
          'Booking Details',
          style:
              const TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView(
        padding:  const EdgeInsetsDirectional.fromSTEB(0,0,0,10),
        children: [
          Container(
            margin: const EdgeInsetsDirectional.fromSTEB(15, 10, 15, 7),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => BookingDetailsScreen(
                              texts: widget.texts,
                            )));
              },
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                  //set border radius more than 50% of height and width to make circle
                ),
                elevation: 7,
                color: Colors.white,
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      (data as Map)["startDate"],
                                      style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      (widget.texts as Map)["departureCode"],
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                ),
                                Image.asset('lib/assets/images/bookings.png'),
                                Column(
                                  children: [
                                    Text(
                                      (data as Map)["endDate"],
                                      style: TextStyle(
                                          fontSize: 22,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      (widget.texts as Map)["destinationCode"],
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                // Text(
                                //   texts[0].replaceAll(' ', '\n'),
                                //   style: TextStyle(
                                //     fontSize: 12,
                                //     color: _greyColor,
                                //   ),
                                // ),
                                Text(
                                  (widget.texts as Map)["departureName"],
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: _greyColor,
                                  ),
                                ),
                                Text(
                                  (widget.texts as Map)["destinationName"],
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: _greyColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Date',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: _greyColor,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        Icon(Icons.calendar_month_outlined),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          (data as Map)["date"],
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Seats',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: _greyColor,
                                      ),
                                    ),
                                    Text(
                                      (widget.texts as Map)["bookedSeats"]
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Flight',
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: _greyColor,
                                      ),
                                    ),
                                    Text(
                                      'FL' +
                                          (widget.texts as Map)["flightId"]
                                              .toString(),
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),

                              ],
                            ),
                          )
                        ],
                      ),
                      Divider(color: Color.fromRGBO(132, 182, 248, 1)),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Price:',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            ),
                            Text(
                              '\$${ (widget.texts as Map)["price"]}',
                              style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w600,
                                  color:
                                  Color.fromRGBO(132, 182, 248, 1)),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          for (var passenger in passengerData.asMap().entries)
            Padding(
              padding: const EdgeInsets.all(22.0),
              child: Column(

                children: <Widget>[

                  Text("Passenger #" + (passenger.key + 1).toString(), style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),),
                  for (var info in passenger.value)
                    Container(
                        margin:
                        EdgeInsets.only(top: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              info.title,
                              textAlign: TextAlign.start,
                              style: const TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color:
                                  Color.fromRGBO(132, 182, 248, 1)),
                            ),
                            const SizedBox(
                              height: 10, // <-- SEE HERE
                            ),
                            Text(
                              info.placeholder,
                              textAlign: TextAlign.start,
                              style: const TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16),
                            ),
                            const Divider(
                                height: 10,
                                thickness: 1,
                                indent: 0,
                                endIndent: 0,
                                color:
                                Color.fromRGBO(132, 182, 248, 0.3)),
                          ],
                        )),
                ],
              ),
            ),
        ],
      ),
    );
  }
}


class InfoItem {
  InfoItem(
      [this.title = '',
        this.placeholder = '',
        this.icon = const Icon(Icons.email)]);

  String title;
  String placeholder;
  Icon icon;
}
