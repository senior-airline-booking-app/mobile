import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PassengerInfoComponent extends StatefulWidget {
  final dynamic passenger;
  final int index;
  final Function(String) onNameChanged;
  final Function(String) onSurnameChanged;
  final Function(String) onPassportChanged;
  final bool validationError;

  const PassengerInfoComponent(
      {Key? key, required this.passenger, required this.index, required this.onNameChanged,
        required this.onPassportChanged,
        required this.onSurnameChanged, required this.validationError})
      : super(key: key);

  @override
  State<PassengerInfoComponent> createState() => _PassengerInfoComponentState();
}

class _PassengerInfoComponentState extends State<PassengerInfoComponent> {
  late dynamic _passenger;
  late dynamic _index;
  double borderWidth = 0.1;
  double columnGap = 15;

  @override
  void initState() {
    _passenger = widget.passenger;
    _index = widget.index;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsetsDirectional.fromSTEB(0, 5, 0, 5),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        elevation: 1,
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
          child: Column(
            children: [
              Center(
                child: Text(
                  'Passenger #${_index+1}',
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              const SizedBox(height: 33),
              TextFormField(
                onChanged: widget.onNameChanged,
                initialValue: widget.passenger.name,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.grey),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueAccent, width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  prefixIcon: const Icon(Icons.person_outline),
                  labelText: 'Name',
                  floatingLabelAlignment: FloatingLabelAlignment.start,
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Enter your name',
                  errorText: widget.validationError && widget.passenger.name == ''
                      ? 'This field is required'
                      : null,
                ),
                validator: (String? value) {
                  // if (value == null || value.isEmpty) {
                  //   return 'required';
                  // }
                  return null;
                },
              ),
              SizedBox(height: columnGap),
              TextFormField(
                onChanged: widget.onSurnameChanged,
                initialValue: widget.passenger.surname,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.grey),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueAccent, width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  prefixIcon: const Icon(Icons.person_outline),
                  labelText: 'Surname',
                  floatingLabelAlignment: FloatingLabelAlignment.start,
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Enter your surname',
                  errorText: widget.validationError && widget.passenger.surname == ''
                      ? 'This field is required'
                      : null,
                ),
                validator: (String? value) {
                  // if (value == null || value.isEmpty) {
                  //   return 'required';
                  // }
                  return null;
                },
              ),
              SizedBox(height: columnGap),
              TextFormField(
                onChanged: widget.onPassportChanged,
                initialValue: widget.passenger.passportId,
                decoration: InputDecoration(
                  hintStyle: const TextStyle(color: Colors.grey),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blueAccent, width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: borderWidth),
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  prefixIcon: const Icon(FontAwesomeIcons.passport),
                  prefixIconColor: Colors.blueAccent,
                  labelText: 'Passport ID',
                  floatingLabelAlignment: FloatingLabelAlignment.start,
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'ED 25265 589',
                  errorText: widget.validationError && widget.passenger.passportId == ''
                      ? 'This field is required'
                      : null,
                ),
                validator: (String? value) {
                  // if (value == null || value.isEmpty) {
                  //   return 'required';
                  // }
                  return null;
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
