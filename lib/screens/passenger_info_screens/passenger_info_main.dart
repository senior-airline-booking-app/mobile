import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/passenger_info_screens/passenger_info_component.dart';
import 'package:senior_project/screens/payment_screens/payment.dart';

import '../../store/flight_store.dart';

// class Passenger {
//   final String name;
//   final String surname;
//   final String passportId;
//
//   Passenger(
//       {required this.name, required this.surname, required this.passportId});
//
//
// }

class PassengerInfoMain extends StatefulWidget {
  const PassengerInfoMain({Key? key, this.num = 1}) : super(key: key);

  final int num;

  @override
  State<PassengerInfoMain> createState() => _PassengerInfoMainState();
}

class _PassengerInfoMainState extends State<PassengerInfoMain> {
  List<Passenger> passengers = [];

  List<bool> validationErrors = [];

  @override
  void initState() {
    super.initState();
    passengers = List<Passenger>.generate(
        widget.num, (_) => Passenger(name: '', surname: '', passportId: ''));
    validationErrors = List<bool>.generate(widget.num, (_) => false);
  }

  void addPassenger(int index, String name, String surname, String passportId) {
    passengers[index] =
        Passenger(name: name, surname: surname, passportId: passportId);
    print('-------passengers');
    print(passengers);
  }

  bool validateFields() {
    bool isValid = true;
    for (int i = 0; i < widget.num; i++) {
      Passenger passenger = passengers[i];
      bool hasError = passenger.name == '' ||
          passenger.surname == '' ||
          passenger.passportId == '';
      validationErrors[i] = hasError;
      if (hasError) {
        isValid = false;
      }
    }
    setState(() {});
    return isValid;
  }

  void redirectToNext() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const PaymentScreen()),
    );
  }

  void redirectBack() {
    Navigator.pop(context);
  }

  void addPassengersToStore(BuildContext context) {
    final flightStore = Provider.of<FlightStore>(context, listen: false);
    flightStore.clearPassengers();
    for (Passenger passenger in passengers) {
      flightStore.addPassenger(passenger);
    }
    print('from store');
    print(flightStore.passengers);
  }

  void handleProceed(BuildContext context) {
    if (validateFields()) {
      addPassengersToStore(context);
      redirectToNext();
    } else {
      // show error message
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          textAlign: TextAlign.center,
          'Passenger Info',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        ),
        elevation: 0,
        backgroundColor: const Color.fromRGBO(249, 251, 250, 1),
        leading: const BackButton(color: Colors.grey),
      ),
      body: Container(
        margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Center(
              child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: passengers.length,
                  itemBuilder: (context, index) {
                    Passenger passenger = passengers[index];
                    return PassengerInfoComponent(
                      index: index,
                      passenger: passengers[index],
                      onNameChanged: (value) {
                        addPassenger(index, value, passenger.surname,
                            passenger.passportId);
                        setState(() {
                          passenger.name = value;
                        });
                      },
                      onSurnameChanged: (value) {
                        addPassenger(
                            index, passenger.name, value, passenger.passportId);
                        setState(() {
                          passenger.surname = value;
                        });
                      },
                      onPassportChanged: (value) {
                        addPassenger(
                            index, passenger.name, passenger.surname, value);
                        setState(() {
                          passenger.passportId = value;
                        });
                      },
                      validationError: validationErrors[index],
                    );
                  },
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 42,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: MaterialStateColor.resolveWith(
                                  (states) =>
                                      const Color.fromRGBO(255, 255, 255, 1)),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              side: const BorderSide(
                                  width: 1,
                                  color: Color.fromRGBO(132, 182, 248, 1)),
                              elevation: 0),
                          onPressed: () {
                            redirectBack();
                          },
                          child: const Text(
                            'Cancel',
                            style: TextStyle(
                                letterSpacing: 1,
                                fontSize: 18,
                                color: Color.fromRGBO(132, 182, 248, 1),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 32,
                    ),
                    Expanded(
                      child: SizedBox(
                        height: 42,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  const Color.fromRGBO(132, 182, 248, 1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              elevation: 0),
                          onPressed: () => handleProceed(context),
                          child: const Text(
                            'Proceed',
                            style: TextStyle(
                                letterSpacing: 1,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          )),
        ),
      ),
    );
  }
}
