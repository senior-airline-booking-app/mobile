import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

// const String baseUrl = 'http://10.0.2.2/:8080/api/v1';
// const String baseUrl = 'https://127.0.0.1:8080/api/v1';
// const String baseUrl = 'http://192.168.0.193:8080/api/v1';
const String baseUrl = 'http://localhost:8080/api/v1';

String adminBasicAuth =
    'Basic ${base64Encode(utf8.encode('admin@gmail.com:asd'))}';

Future<String?> getAuth() async {
  final prefs = await SharedPreferences.getInstance();
  return prefs.getString('token');
}
