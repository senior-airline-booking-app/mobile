import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:senior_project/screens/creation_screens/flight_details.dart';
import 'package:senior_project/screens/splash.dart';
import 'package:senior_project/store/flight_store.dart';
import 'package:senior_project/store/user_store.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final userStore = UserStore();
  await userStore.loadUserFromPrefs();

  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(
    // ChangeNotifierProvider(
    //   create: (context) => UserStore(),
    //   child: const MyApp(),
    // ),
    MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: userStore),
        ChangeNotifierProvider(create: (context) => FlightStore()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          fontFamily: 'Inter', primaryColor: Color.fromRGBO(132, 182, 248, 1)),
      home:
          //const FlightDetailsScreen(),
          const SplashScreen(), // becomes the route named '/'
    );
  }
}
