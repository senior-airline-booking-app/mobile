import 'package:go_router/go_router.dart';
import 'package:senior_project/screens/authorization_screens/login.dart';
import 'package:senior_project/screens/authorization_screens/registration.dart';
import 'package:senior_project/screens/profile_screens/profile.dart';
import 'package:senior_project/screens/profile_screens/profile_edit.dart';
import 'package:senior_project/screens/screens_from_tabs/search_flight_screens/date_picker_screen.dart';
import 'package:senior_project/screens/splash.dart';
import 'package:senior_project/screens/tabs_screen.dart';

final router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) => const SplashScreen(),
    ),
    GoRoute(
      path: '/login',
      builder: (context, state) => const LoginPage(),
    ),
    GoRoute(
      path: '/registration',
      builder: (context, state) => const RegistrationPage(),
    ),
    GoRoute(
      path: '/main',
      builder: (context, state) => const TabsScreen(),
    ),
    GoRoute(
      path: '/date-picker',
      builder: (context, state) => const DatePickerScreen(),
    ),
    GoRoute(
      path: '/profile',
      builder: (context, state) => const ProfilePage(),
    ),
    GoRoute(
      path: '/profileEdit',
      builder: (context, state) => const ProfileEditPage(),
    ),
  ],
);
